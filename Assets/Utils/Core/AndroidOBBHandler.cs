﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Utils.Core
{
    public class AndroidOBBHandler : SingletonObject<AndroidOBBHandler>
    {
        public enum EErrorCode
        { 
            INVALID = -1,
            NO_APP_KEY = 0,
            NOT_ANDROID,
            ALREADY_RUNNING,
            NETWORK,
            NO_SDCARD
        }

        public System.Action<EErrorCode> OnError;
        public System.Action OnCompleted;
        public System.Action<float> OnProgress;

        string base64AppKey { get; set; }
        string _expPath;
        bool _downloadStarted;
#if(UNITY_ANDROID)
        #region Plugin Stuff
        private AndroidJavaClass detectAndroidJNI;
	    private AndroidJavaClass Environment;
	    private const string Environment_MEDIA_MOUNTED = "mounted";

	    bool RunningOnAndroid()
	    {
		    if (detectAndroidJNI == null)
			    detectAndroidJNI = new AndroidJavaClass("android.os.Build");
		    return detectAndroidJNI.GetRawClass() != IntPtr.Zero;
	    }
	

	
	    string GetExpansionFilePath()
	    {
		    populateOBBData();

		    if (Environment.CallStatic<string>("getExternalStorageState") != Environment_MEDIA_MOUNTED)
			    return null;
			
		    const string obbPath = "Android/obb";
			
		    using (AndroidJavaObject externalStorageDirectory = Environment.CallStatic<AndroidJavaObject>("getExternalStorageDirectory"))
		    {
			    string root = externalStorageDirectory.Call<string>("getPath");
			    return String.Format("{0}/{1}/{2}", root, obbPath, obb_package);
		    }
	    }

	    string GetMainOBBPath(string expansionFilePath)
	    {
		    populateOBBData();

		    if (expansionFilePath == null)
			    return null;
		    string main = String.Format("{0}/main.{1}.{2}.obb", expansionFilePath, obb_version, obb_package);
		    if (!File.Exists(main))
			    return null;
		    return main;
	    }

	    string GetPatchOBBPath(string expansionFilePath)
	    {
		    populateOBBData();

		    if (expansionFilePath == null)
			    return null;
		    string main = String.Format("{0}/patch.{1}.{2}.obb", expansionFilePath, obb_version, obb_package);
		    if (!File.Exists(main))
			    return null;
		    return main;
	    }

	    void FetchOBB()
	    {
		    using (AndroidJavaClass unity_player = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		    {
			    AndroidJavaObject current_activity = unity_player.GetStatic<AndroidJavaObject>("currentActivity");
	
			    AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent",
															    current_activity,
															    new AndroidJavaClass("com.unity3d.plugin.downloader.UnityDownloaderActivity"));
	
			    int Intent_FLAG_ACTIVITY_NO_ANIMATION = 0x10000;
			    intent.Call<AndroidJavaObject>("addFlags", Intent_FLAG_ACTIVITY_NO_ANIMATION);
			    intent.Call<AndroidJavaObject>("putExtra", "unityplayer.Activity", 
														    current_activity.Call<AndroidJavaObject>("getClass").Call<string>("getName"));
			    current_activity.Call("startActivity", intent);
	
			    if (AndroidJNI.ExceptionOccurred() != System.IntPtr.Zero)
			    {
				    Debug.LogError("Exception occurred while attempting to start DownloaderActivity - is the AndroidManifest.xml incorrect?");
				    AndroidJNI.ExceptionDescribe();
				    AndroidJNI.ExceptionClear();
			    }
		    }
	    }
	
	    // This code will reuse the package version from the .apk when looking for the .obb
	    // Modify as appropriate
	    private string obb_package;
	    private int obb_version = 0;
	    private void populateOBBData()
	    {
		    if (obb_version != 0)
			    return;
		    using (AndroidJavaClass unity_player = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		    {
			    AndroidJavaObject current_activity = unity_player.GetStatic<AndroidJavaObject>("currentActivity");
			    obb_package = current_activity.Call<string>("getPackageName");
			    AndroidJavaObject package_info = current_activity.Call<AndroidJavaObject>("getPackageManager").Call<AndroidJavaObject>("getPackageInfo", obb_package, 0);
			    obb_version = package_info.Get<int>("versionCode");
		    }
        }
        #endregion 
#endif
        /// <summary>
        /// Set Base 64 string, get it from your google play account, under All Apps -> API & Services
        /// </summary>
        public void SetAppKey(string appKey)
        {
#if(UNITY_ANDROID)
            if (!RunningOnAndroid())
            {
                if (null != OnError) OnError(EErrorCode.NOT_ANDROID);
                return;
            }

            if (!string.IsNullOrEmpty(base64AppKey))
            {
                if (null != OnError) OnError(EErrorCode.ALREADY_RUNNING);
                return;
            }

            _downloadStarted = false;
            base64AppKey = appKey;

            Environment = new AndroidJavaClass("android.os.Environment");

            using (AndroidJavaClass dl_service = new AndroidJavaClass("com.unity3d.plugin.downloader.UnityDownloaderService"))
            {
                // stuff for LVL -- MODIFY FOR YOUR APPLICATION!
                dl_service.SetStatic("BASE64_PUBLIC_KEY", base64AppKey);
                // used by the preference obfuscater
                dl_service.SetStatic("SALT", new byte[] { 1, 43, 256 - 12, 256 - 1, 54, 98, 256 - 100, 256 - 12, 43, 2, 256 - 8, 256 - 4, 9, 5, 256 - 106, 256 - 108, 256 - 33, 45, 256 - 1, 84 });
            }
#else
            Debug.LogWarning("AndroidOBBHandler must be running on Android");
#endif
        }

        public bool StartDownload()
        {
#if(UNITY_ANDROID)
            // check app key
            if (string.IsNullOrEmpty(base64AppKey))
            {
                if (null != OnError) OnError(EErrorCode.NO_APP_KEY);
                return false;
            }

            // check android enviroment
            if (!RunningOnAndroid())
            {
                if (null != OnError) OnError(EErrorCode.NOT_ANDROID);
                return false;
            }

            // check if we have sdcard
            _expPath = GetExpansionFilePath();
            if ( string.IsNullOrEmpty( _expPath ) )
            {
                if (null != OnError) OnError(EErrorCode.NO_SDCARD);
                return false;
            }
            else
            {
                string mainPath = GetMainOBBPath(_expPath);
                string patchPath = GetPatchOBBPath(_expPath);

                if (string.IsNullOrEmpty( mainPath ) )                    
                    FetchOBB();

                StartCoroutine( download() );
            }
#else
            Debug.LogWarning("AndroidOBBHandler must be running on Android");
#endif
            return true;
        }

        private IEnumerator download()
        {
			#if UNITY_ANDROID
            string mainPath;
            do
            {
                yield return new WaitForSeconds(0.5f);
                mainPath = GooglePlayDownloader.GetMainOBBPath(_expPath);
                //log("waiting mainPath " + mainPath);
            }
            while (mainPath == null);

            if (_downloadStarted == false)
            {
                _downloadStarted = true;

                string uri = "file://" + mainPath;
                //log("downloading " + uri);
                WWW www = WWW.LoadFromCacheOrDownload(uri, 0);

                // Wait for download to complete
                while (!www.isDone)
                {
                    if( null != OnProgress ) OnProgress( (www.progress * 100f)/100f );
                    yield return null;
                }

                if (www.error != null)
                {
                    Debug.Log("wwww error " + www.error);
                    if (null != OnError) OnError(EErrorCode.NETWORK);
                }
                else
                {
                    OnCompleted();
                }
            }
			#endif
			yield return null;
        }
    }

}