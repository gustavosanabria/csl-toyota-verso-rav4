﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using Unity.IO.Compression;
using System.Text;

namespace Utils.Core
{

    public class DataBuilder
    {
        FileStream _file;
        BinaryWriter _writer;
        public DataBuilder(string _path)
        {
            _file = File.Open(_path, FileMode.OpenOrCreate);
            _writer = new BinaryWriter(_file);
        }

        public void Add( string data )
        {
            _writer.Write(data);
            _writer.Close();
            _writer = null;
        }

        public void End()
        {
            _file.Close();
            _file.Dispose();
            _file = null;
        }
    }

    public class FileDescription
    {
        public string path { get; set; }
        public string filename { get; set; }
        public string extension { get; set; }
    }

    public class FileHelper
    {
        //===============================================================================
        // FIELDS
        //===============================================================================
        public enum EFolder
        { 
            NONE = 0,
            PERSISTENT,
            STREAMING
        }
        //===============================================================================
        // PROPERTIES
        //===============================================================================

        //===============================================================================
        // PRIVATE METHODS
        //===============================================================================

        //===============================================================================
        // PUBLIC METODS
        //===============================================================================

        public static bool GetDownloadStream( string url, out WWW www, string _folder = null )
        {
            
            string filename = (_folder==null ? "" : (_folder + "/")) + System.IO.Path.GetFileName(url);

            // check if this file was downloaded from internet
            if (Utils.Core.FileHelper.Exists(filename))
            {
                www =  new WWW(Utils.Core.FileHelper.PersistentDataPath(filename, true));
                return false;
            }

            // remote file steaming
            www = new WWW(url);
            return true;
        }

        public static FileDescription GetFileDescription( string path )
        {
            return new FileDescription { 
                path = System.IO.Path.GetDirectoryName(path), 
                filename = System.IO.Path.GetFileNameWithoutExtension(path),
                extension = System.IO.Path.GetExtension(path)
            };
        }

        public static string PersistentDataPath( string file, bool filePath = false )
        {            
            string path = Application.persistentDataPath + "/" + file;

            if (filePath)
            {
                string prefix = "file://";

#if UNITY_WINDOWS || UNITY_EDITOR
                return prefix + "/" + path;
#else
                return prefix + path;
#endif
            }

            return path;
        }

        public static string StreamingDataPath( string file )
        {
            return System.IO.Path.Combine(Application.streamingAssetsPath, file);
        }

        public static string DataPath( EFolder folder, string file )
        {
            if (folder == EFolder.PERSISTENT)
                return System.IO.Path.Combine(Application.persistentDataPath, file);
            else
                return System.IO.Path.Combine(Application.streamingAssetsPath, file);
        }

        public static void CreateFoldersForFile(string file, EFolder folder = EFolder.PERSISTENT )
        { 
            string[] folders = file.Split('/');

            if (folders.Length > 1)
            {
                string basePath = folder == EFolder.PERSISTENT ? Application.persistentDataPath : folder == EFolder.STREAMING ? Application.streamingAssetsPath : "";

                for (int i = 0; i < folders.Length - 1; i++)
                {
                    if (!string.IsNullOrEmpty(folders[i]))
                    {
                        string path = basePath + "/" + folders[i];

                        if (!Directory.Exists(path))
                            Directory.CreateDirectory(path);

                        basePath = path;
                    }
                }
            }

        }
      
        /// <summary>
        /// Copy from streaming assets to persistent folder, Default: no override file if exist.
        /// </summary>
        /// <param name="file">The file to copy</param>
        /// <param name="overwrite">Over write file if exist.</param>
        /// <returns>file path</returns> 
        public static string CopyToPersistent(string file, bool overwrite = false)
        {
            // get the destination file path
            string _path = PersistentDataPath(file);

            // if that file does not exist or must be overwriten
            if (overwrite || !File.Exists(_path))
            {
                // get source file path (located in StreamingAssets folder)
                string _source = StreamingDataPath(file);

                // if that file exist.

                // create all necessary sub-folders inside persistent folder.
                CreateFoldersForFile(file);

                // copy the file from Streaming to Presistent Folder
#if(UNITY_ANDROID && !UNITY_EDITOR)

                WWW www = new WWW(_source);
                    
                //yield return www;
                
                while(!www.isDone)
                {
                    Debug.Log("Download Progress (" + (www.progress * 100.0f).ToString("####0.00") + "%)");
                }

                if (String.IsNullOrEmpty(www.error))
                    System.IO.File.WriteAllBytes(_path, www.bytes);
                else
                {
                    // source file does not exist
                    _path = null;
                    Debug.LogError("File [" + file + "] must exist in StreamingAssets Folder ");
                }
#else
                if (System.IO.File.Exists(_source))
                    System.IO.File.Copy(_source, _path, true);
                else
                {
                    // source file does not exist
                    _path = null;
                    Debug.LogError("File [" + file + "] must exist in StreamingAssets Folder ");
                }
#endif

            }
            return _path;
        }

        /// <summary>
        /// Asynchronous Copy from streaming assets to persistent folder, good for big files, Default: no override file if exist.
        /// </summary>
        /// <param name="file">The file to copy</param>
        /// <param name="callback">Copy finished callback</param>
        /// <param name="overwrite">Over write file if exist.</param>
        /// <returns></returns>
        public static IEnumerator CopyToPersistent( string file, Action<string> callback, bool overwrite = false )
        {
            // get the destination file path
            string _path = PersistentDataPath( file );

            // if that file does not exist or must be overwriten
            if (overwrite || !File.Exists(_path))
            {
                // get source file path (located in StreamingAssets folder)
                string _source = StreamingDataPath(file);

                // if that file exist.
                
                // create all necessary sub-folders inside persistent folder.
                CreateFoldersForFile(file);

                // copy the file from Streaming to Presistent Folder
#if(UNITY_ANDROID && !UNITY_EDITOR)
                Debug.Log("Copying file: " + file);
                WWW www = new WWW(_source);

                while (!www.isDone)
                {
                    //Show some progress, we want an asynchronous download
                    string status = "Downloading " + file + " (" + (www.progress * 100.0f).ToString("####0.00") + "%)";
                    Debug.Log(status);
                    //yield return null;
                }

                if (String.IsNullOrEmpty(www.error))
                    System.IO.File.WriteAllBytes(_path, www.bytes);
                else
                {
                    // source file does not exist
                    _path = null;
                    Debug.LogError("File [" + file + "] must exist in StreamingAssets Folder ");
                }
#else
                if (System.IO.File.Exists(_source))
                    System.IO.File.Copy(_source, _path, true);
                else
                {
                    // source file does not exist
                    _path = null;
                    Debug.LogError("File [" + file + "] must exist in StreamingAssets Folder ");
                }
#endif
                
            }
            yield return null;
            // invoke finish callback
            if (null != callback)
                callback(_path);
        }

        public static string ReadText(string filename)
        {
            string _path = PersistentDataPath(filename);

            if (!File.Exists(_path)) return null;

            StreamReader _stream = new StreamReader(_path);

            string _rv = _stream.ReadToEnd();

            _stream.Close();
            _stream.Dispose();
            _stream = null;

            return _rv;
        }

        public static Byte[] Read(string filename)
        {
            string _path = PersistentDataPath(filename);

            if (!File.Exists(_path)) return null;

            FileStream _file = File.Open(_path, FileMode.Open);

            BinaryReader _reader = new BinaryReader(_file);

            Byte[] _rv = _reader.ReadBytes((int)_file.Length);

            _file.Close();
            _file.Dispose();
            _file = null;

            _reader.Close();
            _reader = null;

            return _rv;
        }

        /// <summary>
        /// Updates or Creates a Persistent File and load it with data, if any.
        /// </summary>
        /// <param name="filename">Path to the Persistent File. Note that this file is saved in the persistent folder.</param>
        /// <param name="data">the binary data. if null the file will be created but will be empty </param>
        public static string Write(string filename, byte[] data )
        {
            string _path = PersistentDataPath(filename);

            if (!File.Exists(_path))
                CreateFoldersForFile(filename);

            FileStream _file = File.Open(_path, FileMode.OpenOrCreate);

            if (null != data)
            {
                BinaryWriter _writer = new BinaryWriter(_file);
                _writer.Write(data);
                _writer.Close();
                _writer = null;
            }

            _file.Close();
            _file.Dispose();
            _file = null;

            return _path;
        }

        public static string Write( string filename, string stringData )
        {
            return Write(filename, System.Text.ASCIIEncoding.ASCII.GetBytes(stringData));
        }
        
        public static DataBuilder BeginWrite(string filename)
        {

            string _path = PersistentDataPath(filename);

            if (!File.Exists(_path))
                CreateFoldersForFile(filename);

            return new DataBuilder(_path);
        }
        
        public static bool Delete( string filename )
        {
            string _path = PersistentDataPath(filename);

            if (File.Exists(_path))
            {
                File.Delete(_path);
                return true;
            }

            return false;
        }

        public static bool Exists( string name, bool isAbsulute = false )
        {
            
            string _path = isAbsulute ? name : PersistentDataPath(name);

            return File.Exists(_path) || Directory.Exists(_path);
        }

        public static string[] GetFiles(string _root, EFolder folder = EFolder.PERSISTENT)
        {
            string _path = DataPath(folder, _root);
            return Directory.GetFiles(_path);
        }

        /*
        static bool DecompressFile(string sDir, GZipStream zipStream, Action<string> progress)
        {
            int _l = 1024;
            //Decompress file name
            byte[] bytes = new byte[_l] ;
            
            int Readed = zipStream.Read(bytes, 0, _l);

            Stream fs = zipStream.BaseStream;

            //Debug.Log(bytes.ToString());
            if (Readed < sizeof(int))
                return false;
            
            //zipStream.
            
            int iNameLen = BitConverter.ToInt32(bytes, 0);
            bytes = new byte[sizeof(char)];
           
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 100; i++)
            {
                zipStream.Read(bytes, 0, sizeof(byte));
                char c = BitConverter.ToChar(bytes, 0);
                sb.Append(c);
            }
            
            string sFileName = sb.ToString();
            if (progress != null)
                progress(sFileName);
            
            //Decompress file content
            bytes = new byte[sizeof(int)];
            zipStream.Read(bytes, 0, sizeof(int));
            int iFileLen = BitConverter.ToInt32(bytes, 0);

            bytes = new byte[iFileLen];
            zipStream.Read(bytes, 0, bytes.Length);

            string sFilePath = Path.Combine(sDir, sFileName);
            string sFinalDir = Path.GetDirectoryName(sFilePath);
            if (!Directory.Exists(sFinalDir))
                Directory.CreateDirectory(sFinalDir);

            using (FileStream outFile = new FileStream(sFilePath, FileMode.Create, FileAccess.Write, FileShare.None))
                outFile.Write(bytes, 0, iFileLen);
            
            return true;
        }

        public static void DecompressToDirectory(string sCompressedFile, string sDir, Action<string> progress)
        {
            //using (FileStream inFile = new FileStream( sCompressedFile, FileMode.Open, FileAccess.Read ))

            string _path = StreamingDataPath(sCompressedFile);

            if (!File.Exists(_path)) return;

            FileStream _file = File.Open(_path, FileMode.Open);

            using (GZipStream zipStream = new GZipStream(_file, CompressionMode.Decompress))
                while (DecompressFile(sDir, zipStream, progress)) ;
        }
        */
    }
}

