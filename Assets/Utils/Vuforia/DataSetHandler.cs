﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Vuforia;

namespace Utils.Vuforia
{
    public abstract class DataSetHandler : MonoBehaviour
    {

        protected DataSet mDataset = null;
        protected ObjectTracker tracker = null;

		public string _dataSet = "CSLVersoDB";

        //[SerializeField]
        protected int _projectID;

        protected abstract void OnDataSetLoaded();

        protected virtual void Start()
        {
			Debug.Log("DataSetHandler Start");

			VuforiaAbstractBehaviour qcar = (VuforiaAbstractBehaviour)FindObjectOfType(typeof(VuforiaAbstractBehaviour));
            if (qcar)
            {
				qcar.RegisterVuforiaStartedCallback(LoadDataSet);
				Debug.Log("Loading DataSet...");
            }
            else
            {
                Debug.Log("Failed to find VuforiaBehaviour in current scene");
            }

        }

        void LoadDataSet()
        {

            // First, create the dataset
            tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
            Utils.Core.FileDescription fd = Utils.Core.FileHelper.GetFileDescription(_dataSet);

            string _realPathXML = "QCAR/" + fd.filename + ".xml";
            string _realPathDAT = "QCAR/" + fd.filename + ".dat";

            foreach (var ds in tracker.GetDataSets())
            {
                Debug.Log( "Loaded DataSets: " + ds.Path );
                if (ds.Path.Contains(_realPathXML))
                {
                    mDataset = ds;
                    break;
                }
            }

            if (null == mDataset)
            {
                mDataset = tracker.CreateDataSet();

                string _pathXML = Utils.Core.FileHelper.CopyToPersistent(_realPathXML);
                string _pathDAT = Utils.Core.FileHelper.CopyToPersistent(_realPathDAT);

                Debug.Log("loading: " + _pathXML);
                if (!mDataset.Load(_pathXML, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
                    Debug.Log("Couldn't load tracking info");
                else
                    tracker.ActivateDataSet(mDataset);
            }

            OnDataSetLoaded();
        }
    }
}