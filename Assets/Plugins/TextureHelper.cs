﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class TextureHelper {
    public static int GetMaxTextureSize()
    {      
#if UNITY_ANDROID
        using (AndroidJavaClass glClass = new AndroidJavaClass("android.opengl.GLES20"))
        {
            using (AndroidJavaClass intBufferClass = new AndroidJavaClass("java.nio.IntBuffer"))
            {
                AndroidJavaObject intBuffer = intBufferClass.CallStatic<AndroidJavaObject>("allocate", 1);

                int MAX_TEXTURE_CODE = glClass.GetStatic<int>("GL_MAX_TEXTURE_SIZE");
                glClass.CallStatic("glGetIntegerv", MAX_TEXTURE_CODE, intBuffer);

                return intBuffer.Call<int>("get", 0);
            }
        }
#else
		return 2048;
#endif
    }
}
