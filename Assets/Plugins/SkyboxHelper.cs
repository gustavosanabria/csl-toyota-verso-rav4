﻿using UnityEngine;
using System.Collections;

public class SkyboxHelper
{
    private static string _skyboxVerso_HD = "skyboxVerso@4096";
    private static string _skyboxVerso_Default = "skyboxVerso@2048";

    public static void LoadFromResources()
    {
        string skybox_name = (TextureHelper.GetMaxTextureSize() > 2048) ? _skyboxVerso_HD : _skyboxVerso_Default;
        Debug.Log("Loading skybox " + skybox_name);

        RenderSettings.skybox = Resources.Load<Material>(skybox_name);
    }
}
