﻿using UnityEngine;
using System.Collections;

public class TFTTutorialManager : MonoBehaviour
{
	[SerializeField] GameObject tftIconsContainer;
	[SerializeField] UICamera mainMenuCam;
	[SerializeField] GameObject tftTutContainer;
	Tutorial currentTut;
	[SerializeField] UITexture tutStepUITex;
	[SerializeField] GameObject tutHandlerGO;
	sbyte currentTutStepIndex;
	byte currentTutStepCount;
	[SerializeField] AudioSource audioSrc;
	[SerializeField] AudioClip introClip;
	[SerializeField] GameObject nextStepArrow, previousStepArrow;
	[SerializeField] GameObject returnBtn;

	void Start()
	{
		//EnableTFTTutorial();
		//SetTutorial(tutHandlerGO.GetComponent<TutorialsIDS>().TUT_TFT_TUT_6);
	}

	public void EnableTFTTutorial()
	{
		mainMenuCam.enabled = false;
		tftTutContainer.SetActive(true);
		PlayAudio(introClip);
	}

	public void ShowTFTCategory()
	{
		tftIconsContainer.SetActive(true);
		//audioSrc.Stop();
		PlayAudio(introClip);
	}

	public void CloseTFTCategory()
	{
		tftIconsContainer.SetActive(false);
	}

	public void SetTutorial(byte tutID)
	{
		CloseTFTCategory();

		AnimateReturnButton(false);
		currentTut = tutHandlerGO.GetComponent<TutorialsHandler>().GetTutorial(tutID);
		currentTutStepCount = currentTut.Count();
		currentTutStepIndex = -1;
		NextStep();
	}

	public void NextStep()
	{
		if(currentTutStepIndex == currentTutStepCount-1) { return; }

		ChangeStep(1);
	}

	public void PreviousStep()
	{
		if(currentTutStepIndex == 0) { return; }

		ChangeStep(-1);
	}

	void ChangeStep(sbyte dir)
	{
		//nextStepArrow.SetActive(currentTutStepIndex+dir < currentTutStepCount-1 ? true : false);
		//previousStepArrow.SetActive(currentTutStepIndex+dir > 0? true : false);
		if (currentTutStepIndex + dir < currentTutStepCount - 1)
		{
			EnableButton (nextStepArrow, true);
			AnimateReturnButton(false);
		}
		else 
		{
			EnableButton(nextStepArrow, false);
			AnimateReturnButton(true);
		}

		if(currentTutStepIndex+dir > 0) EnableButton(previousStepArrow, true);
		else EnableButton(previousStepArrow, false);

		currentTutStepIndex += dir;

		audioSrc.Stop();
		tutStepUITex.mainTexture = currentTut.LoadTexture(currentTutStepIndex);
		PlayAudio();
	}

	TweenScale twS;
	void AnimateReturnButton(bool b)
	{
		twS = returnBtn.GetComponent<TweenScale>();

		if (b)
		{
			twS.enabled = true;
			twS.style = UITweener.Style.Loop;
			twS.Play ();
		}
		else
		{
			twS.enabled = false;
			twS.ResetToBeginning();
			twS.style = UITweener.Style.Once;
			returnBtn.transform.localScale = twS.from;
		}
	}

	void EnableButton(GameObject button, bool b)
	{
		if(button.activeInHierarchy && !b)
		{
			button.GetComponent<TweenScale>().PlayForward();
			button.GetComponent<TweenScale>().ResetToBeginning();
			button.GetComponent<UISprite>().enabled = b;
			button.GetComponent<UIButton>().enabled = b;
		}

		button.GetComponent<UISprite>().enabled = b;
		button.GetComponent<UIButton>().enabled = b;
	}

	bool toggleAudio = true;
	[SerializeField] UISprite audioBtnSpr;
	public void ToggleAudio()
	{
		toggleAudio = !toggleAudio;
		PlayAudio();
	}
	
	public void PlayAudio(AudioClip clip = null)
	{
		if(!toggleAudio) audioSrc.Stop();
		else 
		{
			audioSrc.Stop();
			audioSrc.clip = clip ? clip : currentTut.LoadAudio(currentTutStepIndex);
			//audioSrc.clip = currentTut.LoadAudio(currentTutStepIndex); // currentTutClips[currentTutStepIndex];
			audioSrc.Play();
		}

		//audioBtnSpr.spriteName = toggleAudio ? "SoundButton" : "NosoundButton";
	}

	public void BackToScene()
	{
		if (AppLibrary.TFTSourceScene == 0) return;

		if(AppLibrary.TFTSourceScene == AppLibrary.SCENE_VERSO_360)GetComponent<HomeScreenManager>().ShowVRLoading();
		else GetComponent<HomeScreenManager>().ShowARLoading();
	}

	void Update()
	{
		audioBtnSpr.spriteName = toggleAudio ? "SoundButton" : "NosoundButton";
	}
}
