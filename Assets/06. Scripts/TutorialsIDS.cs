﻿using UnityEngine;
using System.Collections;

public class TutorialsIDS : MonoBehaviour
{
	public sbyte TUT_STEP1 = 1;
	public sbyte TUT_STEP2 = 2;
	public sbyte TUT_STEP3 = 3;
	public sbyte TUT_STEP4 = 4;
	public sbyte TUT_STEP5 = 5;
	public sbyte TUT_STEP6 = 6;
	public sbyte TUT_STEP7 = 7;
	public sbyte TUT_STEP8 = 8;
	public sbyte TUT_STEP9 = 9;
	public sbyte TUT_STEP10 = 10;
	public sbyte TUT_STEP11 = 11;
	public sbyte TUT_STEP12 = 12;
	public sbyte TUT_STEP13 = 13;

	public byte TUT_PHONE_BLUETOOTH = 0;
	public byte TUT_PHONE_INTERNET = 1;
	public byte TUT_PHONE_TUT_3 = 2;
	public byte TUT_PHONE_TUT_4 = 3;

	public byte TUT_GPS_TUT_1 = 4;
	public byte TUT_GPS_TUT_2 = 5;
	public byte TUT_GPS_TUT_3 = 6;
	public byte TUT_GPS_TUT_4 = 7;
	public byte TUT_GPS_TUT_5 = 8;

	public byte TUT_APP_TUT_1 = 9;

	public byte TUT_RADIO_TUT_1 = 10;

	public byte TUT_TFT_TUT_1 = 11;
	public byte TUT_TFT_TUT_2 = 12;
	public byte TUT_TFT_TUT_3 = 13;
	public byte TUT_TFT_TUT_4 = 14;
	public byte TUT_TFT_TUT_5 = 15;
	public byte TUT_TFT_TUT_6 = 16;
}
