﻿using UnityEngine;
using System.Collections;
using System.IO;

public class FileManager : MonoBehaviour
{
	#if UNITY_ANDROID
	string sourcePath;
	string dBName = "CSLVersoDB";

	string destinationPath = "";

	void Awake()
	{
		print (Application.persistentDataPath);
		//sourcePath = Application.dataPath + "/Resources/VuforiaDB/CSLVersoDB";
		sourcePath = GooglePlayDownloader.GetExpansionFilePath() + "/assets/QCAR/CSLVersoDB";

		destinationPath = Application.persistentDataPath + "/files/" + dBName;

		if(!Directory.Exists(Application.persistentDataPath + "/files/"))
		{
			//Directory.CreateDirectory(Application.persistentDataPath + "/files/");
		}
	
		//StartCoroutine("CopyXML");
		//StartCoroutine("CopyDAT");

		CopyXML ();
		CopyDAT ();
	}
	

	//IEnumerator CopyXML()
	void CopyXML()
	{
		Utils.Core.FileHelper.CopyToPersistent("QCAR/CSLVersoDB.xml", true);

		if (File.Exists(sourcePath + ".xml"))
		{
			//WWW xml = new WWW(sourcePath + ".xml");
			//yield return xml;

			//File.WriteAllBytes(destinationPath + ".xml", xml.bytes);

			//File.Copy(sourcePath + ".xml", destinationPath + ".xml", true);
		}
	}

	//IEnumerator CopyDAT()
	void CopyDAT()
	{
		Utils.Core.FileHelper.CopyToPersistent("QCAR/CSLVersoDB.dat", true);

		if (File.Exists(sourcePath + ".dat"))
		{
			//WWW dat = new WWW(sourcePath + ".dat");
			//yield return dat;
			
			//File.WriteAllBytes(destinationPath + ".dat", dat.bytes);

			//File.Copy(sourcePath + ".dat", destinationPath + ".dat", true);
		}
	}
	#endif
}
