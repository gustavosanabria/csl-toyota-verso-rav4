﻿using UnityEngine;
using System.Collections;

public class VRCameraClamper : MonoBehaviour
{
	[SerializeField] CardboardHead cBHead;
	[SerializeField] Transform headTrans;
	[SerializeField] TextMesh cBHeadRotTM;
	[SerializeField] float maxUPRot, maxDownRot;
	[SerializeField] bool debugRotation;
	[SerializeField] bool updateRotation;
	[SerializeField] bool SmoothRotation, clampRotation, doubleTapZoom;
	float newX, xRot, xRot2, xLimit, originalMaxUpRot, originalMaxDownRot;
	Vector3 newVector, headRot, headRot2;
	[SerializeField] Texture2D gyroBtnTex, swipeBtnTex;
	[SerializeField] UITexture vrControlModeBtn;

	void Awake()
	{
		updateRotation = true;

		if(AppLibrary.sceneLoad<1)	//ASQUEROSIDAD PARA QUE FUNCIONE DESDE LA PRIMERA CARGA DE ESCENA.
		{							//FURIA WILL NOT APPROVE. NEITHER WILL BORRO.
			AppLibrary.sceneLoad++;
			Application.LoadLevel(Application.loadedLevel);
		}

		originalMaxUpRot = maxUPRot;
		originalMaxDownRot = maxDownRot;

		cBHead = GetComponentInChildren<CardboardHead>();

		if(!debugRotation) { cBHeadRotTM.gameObject.SetActive(false); }

		headRot2 = Cardboard.SDK.HeadPose.Orientation.eulerAngles;

		xRot2 = headRot2.x;

		newX = Mathf.Abs(maxDownRot-xRot2) < Mathf.Abs(maxUPRot-xRot2) ? maxDownRot * .9f : maxUPRot * .9f;

		if((xRot2 > maxDownRot && xRot2 < maxUPRot) || (xRot2 < maxUPRot && xRot2 > maxDownRot))
		{
			headTrans.rotation = Quaternion.Euler(newX, headRot2.y, headRot2.z);
			cBHeadRotTM.color = Color.blue;
			cBHeadRotTM.text = ("Head Rot: " + headTrans.eulerAngles.ToString());
		}

		//headTrans.rotation = Quaternion.Euler(Vector3.zero);
	}

	[SerializeField] bool enableGyro = true;
	public void EnableGyro()
	{
		return;

		enableGyro = !enableGyro;

		vrControlModeBtn.mainTexture = enableGyro ? gyroBtnTex : swipeBtnTex;

		GetComponentInChildren<TouchController>().enabled = !enableGyro;

		updateRotation = enableGyro;
	}

	public void ActivateModeSwitchBtn(bool b)
	{
		vrControlModeBtn.gameObject.SetActive(b);
	}

	public void EnableSwipeNavigation(bool b)
	{
		if(!enableGyro)
		{
			GetComponentInChildren<TouchController>().enabled = b;
		}
	}

	float GetLimit(float currentX)
	{
		return Mathf.Abs(maxDownRot-currentX) < Mathf.Abs(maxUPRot-currentX) ? maxDownRot : maxUPRot;
	}
	
	byte tapCount;
	Touch t,t1,t2;
	bool zoomIn;	 
	float tolerance, prevDistance, distance;
	Vector3 t1Pos, t2Pos, posibleRotation;

	void Update()
	{
		if(Input.touchCount == 1)
		{
			if(doubleTapZoom)
			{
				t = Input.touches[0];

				if(t.phase == TouchPhase.Ended)
				{
					tapCount++;

					if(tapCount == 2){ Zoom(); return; }

					CancelInvoke();
					Invoke("ClearTapCount", .3f);
				}
			}
		}
		else if(Input.touchCount == 2)
		{
			t1 = Input.touches[0];
			t2 = Input.touches[1];

			if(t1.phase != TouchPhase.Moved && t2.phase != TouchPhase.Moved) return;
			SmoothZoom(Vector3.Distance(t1.position, t2.position));
		}
	}

	void SetZoom(bool pZoom)
	{
		zoomIn = pZoom;

		Camera.main.fieldOfView = zoomIn ? 27 : 60;
		maxUPRot = zoomIn ? 325 : originalMaxUpRot;
		maxDownRot = zoomIn ? 35 : originalMaxDownRot;
	}

	void ClearTapCount() { tapCount = 0; }

	void SmoothZoom(float distance){
		if(distance > 700) distance = 700;
		if(distance < 200) distance = 200;
		distance -= 200;
		float value = ((1 - (distance/500)) * 33) +27;
		Camera.main.fieldOfView = value;
	}
	void Zoom()
	{
		CancelInvoke();
		ClearTapCount();

		zoomIn = !zoomIn;
		//Camera.main.fieldOfView = zoomIn ? 40 : 60;
		//maxUPRot = zoomIn ? 335 : originalMaxUpRot;
		//maxDownRot = zoomIn ? 25 : originalMaxDownRot;
		//Camera.main.fieldOfView = zoomIn ? 30 : 60;
		//maxUPRot = zoomIn ? 328 : originalMaxUpRot;
		//maxDownRot = zoomIn ? 32 : originalMaxDownRot;
		Camera.main.fieldOfView = zoomIn ? 27 : 60;
		maxUPRot = zoomIn ? 325 : originalMaxUpRot;
		maxDownRot = zoomIn ? 35 : originalMaxDownRot;
	}
	
	public void LateUpdate()
	{
		//Cardboard.SDK.UpdateState();

		if(!updateRotation) {return;}

		headRot = Cardboard.SDK.HeadPose.Orientation.eulerAngles;
		xRot = headRot.x;

		//if(clampRotation)
		{
			if((xRot < maxDownRot && xRot > 0) || (xRot > maxUPRot && xRot < 360.1f)) 
			//if(xRot < maxDownRot && xRot < maxUPRot) 
			{
				updateRotation = true;
				cBHeadRotTM.color = Color.green;
			}
			else 
			//if(xRot > maxDownRot && xRot < maxUPRot)
			{
				xLimit = GetLimit(xRot);
				newVector = new Vector3(xLimit, headRot.y, headRot.z);
				headRot = newVector;
				cBHeadRotTM.color = Color.red;
			}
		}

		if(updateRotation)
		{
			if(SmoothRotation) headTrans.rotation = Quaternion.Slerp(headTrans.rotation, Quaternion.Euler(headRot), Time.deltaTime * 8);
			else headTrans.rotation = Quaternion.Euler(headRot);
		}
	
		if(!debugRotation) {return;}

		cBHeadRotTM.text = ("Head Rot: " + headTrans.eulerAngles.ToString());
	}
}
