﻿using UnityEngine;
using System.Collections;

public class ToCenter : MonoBehaviour 
{
	private Vector3 currentHairPos;
	private Vector3 target, fwd, point, toPos, originalPos, targetScale, targetRot, originalScale;
	public RaycastHit hit;
	public GameObject camara;
	[SerializeField] bool doTween, slide, matchSize;
	[SerializeField] float offset, slideSpeed, scaleFactor;
	float originalZ, distance;

	void Awake()
	{
		originalPos = transform.localPosition;
		originalZ = transform.position.z;
		originalScale = transform.localScale;
		print(originalScale);
	}

	void Update () 
	{
		fwd = camara.transform.forward;
		//Debug.DrawRay (transform.position, fwd,Color.white);

		if (Physics.Raycast (camara.transform.position, fwd, out hit, 9999, 1<<11)) 
		{
			//print("hit VF target");

			//Debug.DrawRay (camara.transform.position, hit.point, Color.red);

			targetRot = hit.transform.eulerAngles;
			targetScale = hit.transform.lossyScale;

			toPos = hit.transform.position - new Vector3(0,0,offset);
			//toPos = new Vector3 (hit.transform.position.x, hit.transform.position.y, originalZ);

			slide = true;
		}
		else
		{
			slide = false;
			//transform.localPosition = new Vector3(originalPos.x, originalPos.y, transform.localPosition.z);
			transform.localPosition = originalPos;
			transform.localScale = originalScale;
			transform.localEulerAngles = Vector3.zero;
		}

		if(slide)
		{
			transform.position = Vector3.Slerp(transform.position, toPos, Time.deltaTime * slideSpeed);
			distance = Vector3.Distance(transform.position, toPos);

			if(distance < .25f)
			{
				slide = false;
				transform.eulerAngles = targetRot;
				if(matchSize) transform.localScale = targetScale * scaleFactor;
			}
		}
	}
}
