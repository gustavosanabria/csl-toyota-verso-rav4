﻿using UnityEngine;
using System.Collections;

public class HomeScreenManager : MonoBehaviour
{
	[SerializeField]UITexture btnAssistanceUITex;
	[SerializeField]Texture2D btnAssistanceGLOWTex;
    [SerializeField]UITexture btn360ViewUITex;
    [SerializeField]Texture2D btn360ViewGLOWTex;
	[SerializeField]UITexture btnLexiqueUITex;
    [SerializeField]UITexture btnARUITex;  
    [SerializeField]Texture2D btnARGLOWTex;
    [SerializeField]Texture2D btnLexiqueGLOWTex;
    [SerializeField]UITexture btnTUTUITex;
    [SerializeField]Texture2D btnTUTGLOWTex;
	[SerializeField]UITexture btnLegalsUITex;
	[SerializeField]Texture2D btnLegalsGLOWTex;
	[SerializeField]GameObject lexiqueFlashGO;
	[SerializeField]GameObject arFlashGO;
	[SerializeField]GameObject v360FlashGO;
	[SerializeField]GameObject tutFlashGO;
	Texture2D btnAssitanceNormalTex;
	Texture2D btnARNormalTex;
	Texture2D btn360NormalTex;
	Texture2D btnTUTNormalTex;
	Texture2D btnLegalsNormalTex;

	[SerializeField]GameObject tutoScreen;
	[SerializeField]UITexture arHelpFrameTex;
	[SerializeField]GameObject arHelpText;
	[SerializeField]GameObject goToARBtn, backToMenuBtn, backToMenuVRBtn, goToVRBtn;
	[SerializeField]Texture2D arHelpTex;
	[SerializeField]Texture2D arTorchTex;
	[SerializeField]GameObject assistanceScreen, arHelpScreen, vrHelpScreen, legalsScreen;
	[SerializeField]GameObject v360HelpScreen;

	[SerializeField] UIPanel tutBtnsPanel;

	Vector2 originalTutButtonsPanelClipPos;
	Vector3 originalTutButtonsPanelPos;
	[SerializeField] UISpriteAnimation backGroundAnim;

	[SerializeField]GameObject loadingBar;
	[SerializeField]GameObject loadingFrame;

	[SerializeField]GameObject vRLoadingBar;
	[SerializeField]GameObject vRLoadingFrame;

	[SerializeField] UILabel arLoadingLabel;

	bool showARProgressBar, showVRProgressBar;
	float barProgress;

	[SerializeField]GameObject[] flashs;
	
	[SerializeField] UIButton tutBtn;
	[SerializeField] UIButton tutWarningOKBtn;
	[SerializeField] UIButton tutCatPhoneBtn;
	[SerializeField] UIButton tutCatGPSBtn;
	[SerializeField] UIButton tutCatAppBtn;
	[SerializeField] UIButton tutCatRadioBtn;

	[SerializeField] UICamera uiCam;

    void Awake()
    {
        Resources.UnloadUnusedAssets();
        System.GC.Collect();

		loadingFrame.SetActive (false);
		loadingBar.GetComponent<UITexture> ().fillAmount = 0;
		vRLoadingBar.GetComponent<UITexture> ().fillAmount = 0;


		originalTutButtonsPanelClipPos = tutBtnsPanel.clipOffset;
		originalTutButtonsPanelPos = tutBtnsPanel.transform.localPosition;

		btnAssitanceNormalTex = (Texture2D)btnAssistanceUITex.mainTexture;
		btnARNormalTex = (Texture2D)btnARUITex.mainTexture;
		btn360NormalTex = (Texture2D)btn360ViewUITex.mainTexture;
		btnTUTNormalTex = (Texture2D)btnTUTUITex.mainTexture;
		btnLegalsNormalTex = (Texture2D)btnLegalsUITex.mainTexture;

		flashs = new GameObject[]{arFlashGO, v360FlashGO, tutFlashGO};

		if(AppLibrary.openTutAtomatically)
		{
			tutBtn.SendMessage("OnClick");

			AppLibrary.openTutAtomatically = false;
		}

		if(AppLibrary.openTFTTutAtomatically)
		{
			GetComponent<TFTTutorialManager>().EnableTFTTutorial();
			AppLibrary.openTFTTutAtomatically = false;
		}

		if(AppLibrary.AR_HELPVIDEO_PLAYED)
		{
			JumpToARHelp();
		}

		if (AppLibrary.TUT_TO_OPEN.Length > 0) {	//MOD GLOSSARY V.2
			string tutCat = AppLibrary.TUT_TO_OPEN;
			UIButton catBtn;
			
			tutBtn.SendMessage ("OnClick");
			tutWarningOKBtn.SendMessage ("OnClick");
			
			if (tutCat != "main") {
				catBtn = tutCat == "phone" ? tutCatPhoneBtn :
					tutCat == "gps" ? tutCatGPSBtn :
						tutCat == "app" ? tutCatAppBtn :
						tutCat == "radio" ? tutCatRadioBtn :
						null;
				
				catBtn.SendMessage ("OnClick");
			}
		} 
		else 
		{
			GlossaryMgr.LASTDEFWORD = "";	//MOD GLOSSARY V.2
		}
		
		GlossaryMgr.vrTargetsToShow.Clear();	//MOD GLOSSARY V.2
		AppLibrary.TUT_TO_OPEN = "";	
    }

	public void EnableHomeUICamera(bool b)
	{
		uiCam.enabled = b;
	}

	void Start()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		
		print("GC Collected...");
	}

	public void JumpToARHelp()
	{
		arHelpScreen.SetActive(true);
		arHelpScreen.GetComponent<TweenScale> ().enabled = false;
		arHelpScreen.GetComponent<TweenPosition> ().enabled = false;
		arHelpScreen.transform.localScale = Vector3.one;
		arHelpScreen.transform.position = Vector3.zero;
		EnableButtons(false);
		//EnableFlash(arFlashGO);
	}
	
	public void ToMainMenu()
	{
		Start();

		DisableAllFlashs();
		EnableButtons(true);

		btnAssistanceUITex.mainTexture = btnAssitanceNormalTex;
		btnARUITex.mainTexture = btnARNormalTex;
		btn360ViewUITex.mainTexture = btn360NormalTex;
		btnTUTUITex.mainTexture = btnTUTNormalTex;
		btnLegalsUITex.mainTexture = btnLegalsNormalTex;

		AppLibrary.AR_HELPVIDEO_PLAYED = false;
	}

	public void CloseTutoWindow()
	{
		tutoScreen.transform.localScale = tutoScreen.transform.position = Vector3.zero;
		tutBtnsPanel.clipOffset = originalTutButtonsPanelClipPos;
		tutBtnsPanel.transform.localPosition = originalTutButtonsPanelPos;
	}

	public void PlayBackgroundFlash(UISpriteAnimation anim)
	{
		CancelInvoke("PlayFlash");
		backGroundAnim = anim;
		InvokeRepeating("PlayFlash", .5f, 3f);
	}

	void PlayFlash()
	{
		backGroundAnim.ResetToBeginning();
		backGroundAnim.Play();
	}

	public void CloseAssistanceWindow()
	{
		ToMainMenu();
		assistanceScreen.transform.localScale = assistanceScreen.transform.position = Vector3.zero;
		assistanceScreen.SetActive(false);  
	}

    public void CallNumber1()
    {
        Application.OpenURL("tel:" + AppLibrary.AssistancePhone1);
    }

	public void CallNumber1B()
	{
		Application.OpenURL("tel:" + AppLibrary.AssistancePhone1B);
	}

    public void CallNumber2()
    {
        Application.OpenURL("tel:" + AppLibrary.AssistancePhone2);
    }

	public void CallNumber2B()
	{
		Application.OpenURL("tel:" + AppLibrary.AssistancePhone2B);
	}

    public void CloseARHelpWindow()
	{
		arHelpScreen.transform.localScale = arHelpScreen.transform.position = Vector3.zero;
        arHelpScreen.SetActive(false);
	}

	public void Close360HelpWindow()
	{
		v360HelpScreen.transform.localScale = v360HelpScreen.transform.position = Vector3.zero;
        v360HelpScreen.SetActive(false);
	}

	public void ButtonAssistanceCLICK()
	{
		assistanceScreen.SetActive(true);
		btnAssistanceUITex.mainTexture = btnAssistanceGLOWTex;
		EnableButtons(false);
	}

	public void ButtonLegalsCLICK()
	{
		legalsScreen.SetActive(true);
		btnLegalsUITex.mainTexture = btnLegalsGLOWTex;
		EnableButtons(false);
	}

	public void CloseLegalsWindow()
	{
		ToMainMenu();
		legalsScreen.transform.localScale = legalsScreen.transform.position = Vector3.zero;
		legalsScreen.SetActive(false);  
	}

    void ShowARHelpVideo()
    {
		AppLibrary.CURRENTVIDEONAME = AppLibrary.VIDEO_HELP_AR;
        //Application.LoadLevelAdditive(AppLibrary.SCENE_AR_HELPVIDEO);
        Application.LoadLevel(AppLibrary.SCENE_AR_HELPVIDEO);
    }

    public void ButtonARCLICK()
    {
		if (!AppLibrary.AR_HELPVIDEO_PLAYED)
		{
			EnableHomeUICamera(false);
			Invoke ("ShowARHelpVideo", .5f);
		}

		arHelpScreen.SetActive(true);
		btnARUITex.mainTexture = btnARGLOWTex;
		EnableButtons(false);
		
		EnableFlash(arFlashGO);
    }

	public void ButtonLexiqueCLICK()
	{	
		btnLexiqueUITex.mainTexture = btnLexiqueGLOWTex;
		EnableButtons(false);
		
		EnableFlash(lexiqueFlashGO);
		Invoke("LoadGlossary", .5f);
	}

	void LoadGlossary()
	{
		Application.LoadLevel(AppLibrary.SCENE_GLOSSARY);
	}

    public void Button360ViewCLICK()
    {
		v360HelpScreen.SetActive(true);
        btn360ViewUITex.mainTexture = btn360ViewGLOWTex;
		EnableButtons(false);

		EnableFlash(v360FlashGO);
    }

	public void OnHelpTextSlided()
	{
		arHelpFrameTex.enabled = true;
		arHelpFrameTex.mainTexture = arHelpTex;
		EnableGotoARbtn(true);
	}

	void EnableGotoARbtn(bool b)
	{
		goToARBtn.SetActive(b);
		arHelpScreen.GetComponent<UIButton>().enabled = b;
	}
	
    public void ButtonTutorialCLICK()
    {
        btnTUTUITex.mainTexture = btnTUTGLOWTex;
		EnableButtons(false);
		
		EnableFlash(tutFlashGO);
    }

	void EnableButtons(bool b)
	{
		btnAssistanceUITex.GetComponent<Collider>().enabled = b;
		btnLexiqueUITex.GetComponent<Collider>().enabled = b;
		btnARUITex.GetComponent<Collider>().enabled = b;
		btn360ViewUITex.GetComponent<Collider>().enabled = b;
		btnTUTUITex.GetComponent<Collider>().enabled = b;
		btnLegalsUITex.GetComponent<Collider> ().enabled = b;
	}

	void EnableFlash(GameObject go)
	{
		go.GetComponent<UISprite>().enabled = true;
		go.GetComponent<UISpriteAnimation>().enabled = true;
		go.GetComponent<UISpriteAnimation>().ResetToBeginning();
		go.GetComponent<UISpriteAnimation>().Play();
	}

	void DisableAllFlashs()
	{
		for (int i = 0; i < 3; i++)
		{
			flashs[i].GetComponent<UISprite>().enabled = false;
			flashs[i].GetComponent<UISpriteAnimation>().Pause();
			flashs[i].GetComponent<UISpriteAnimation>().enabled = false;
		}
	}

	public void ShowVRLoading()
	{
		LoadingScript.SetLevelToLoad(GetComponent<MySceneManager>().GetVRScene());

		vRLoadingFrame.SetActive(true);
		backToMenuVRBtn.SetActive(false);
		vrHelpScreen.GetComponent<UIButton>().enabled = false;
		goToVRBtn.SetActive(false);

        GetComponent<TutorialManager>().DestroyMe();

		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		
		Application.LoadLevel(AppLibrary.SCENE_LOADING);
	}
	
	public void ShowARLoading()
	{
		LoadingScript.SetLevelToLoad(GetComponent<MySceneManager>().GetARScene());

		arHelpFrameTex.mainTexture = arTorchTex;
		loadingFrame.SetActive (true);

		backToMenuBtn.SetActive(false);
		goToARBtn.SetActive(false);

        GetComponent<TutorialManager>().DestroyMe();

		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		AppLibrary.AR_HELPVIDEO_PLAYED = false;
		Application.LoadLevel(AppLibrary.SCENE_LOADING);
	}

	void GoToVrBitches()
	{
		GetComponent<MySceneManager>().GoTo360ViewScreen();
	}

	void GoToArBitches()
	{
		GetComponent<MySceneManager>().GoToARScreen();
	}
}
