﻿using UnityEngine;
using System.Collections;

public class Cleaner : MonoBehaviour
{

	void Awake()
	{
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
		print("CLEAN!");
	}
}
