﻿using UnityEngine;
using System.Collections;

public class MagnifierScript : MonoBehaviour
{
	[SerializeField] bool magnify;
	[SerializeField] Camera guiCamera;
	Vector3 firstFingerPos;
	Vector3 currentFingerPos;
	Vector3 direction, colPoint;
	RaycastHit rcH;
	[SerializeField] GameObject magnifierCamera, magnifierContainer;
	[SerializeField] UITexture magnifierFrameUiTex;
	float originalZ;
	[SerializeField] bool isVr;
	[SerializeField] OldGyroscopeController gyroControl;
	float magnifierFactor;
	UITexture textureToMagnify;
	DescriptionsManager descrMgr;

	void Awake()
	{
		descrMgr = GetComponent<DescriptionsManager>();
		//originalZ = magnifierCamera.transform.position.z;
	}

	void Start()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();

		print("GC Collected...");
	}

	public void Magnify()
	{
		magnify = !magnify;
		/*magnifierCamera.SetActive(magnify);
		if(magnifierContainer) { magnifierContainer.SetActive(magnify); }
		magnifierFrameUiTex.enabled = magnify;

		magnifierCamera.transform.position = Vector3.zero + new Vector3(0,0,originalZ);*/
	}

	void ForceMagnifierActive()
	{
		magnify = true;
	}

	Vector2 initSize;
	public void SetMagnifierZoom(float zoom = 1f)
	{
		if(zoom == 0) zoom = 1f;
		//print(zoom);

		if(descrMgr.currDescriptionUITex) 
		{
			descrMgr.currDescriptionUITex.width = (int)(descrMgr.currDescrOriginalWidth * (zoom));
			descrMgr.currDescriptionUITex.height = (int)(descrMgr.currDescrOriginalHeight * (zoom));
		}
	}

	public void DisableMagnifier()
	{
		magnify = false;

		/*magnifierCamera.SetActive(false);
		if(magnifierContainer) { magnifierContainer.SetActive(false); }
		magnifierFrameUiTex.enabled = false;
		
		magnifierCamera.transform.position = Vector3.zero + new Vector3(0,0,originalZ);*/
	}

	bool magnifierEnabled;
	public void MagnifierActive(bool b)
	{
		magnifierEnabled = b;

		if(isVr && gyroControl) 
		{
			gyroControl.enabled = !magnifierEnabled;
		}
	}

	Touch t1,t2;

	float zoomValue, offsetX, offsetYMin, offsetYMax;
	void SmoothZoom(Touch touchZero, Touch touchOne)
	 {

		/*distance = Mathf.Clamp(distance, 200, 700) - 200;
		zoomValue = (((distance/500)) * 1f) + .5f;
		zoomValue = Mathf.Clamp (zoomValue, 1f,  1.5f);*/

		float perspectiveZoomSpeed= 0.001f;
		// The rate of change of the field of view in perspective mode.
		// Find the position in the previous frame of each touch.
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		
		// Find the magnitude of the vector (the distance) between the touches in each frame.
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		
		// Find the difference in the distances between each frame.
		float deltaMagnitudeDiff =  touchDeltaMag - prevTouchDeltaMag;
		
		// fov between 35 and 75
		//zoomValue = Camera.main.fieldOfView;
		zoomValue += deltaMagnitudeDiff * perspectiveZoomSpeed;
		//fov = Mathf.Clamp(fov, 35.0f, 75.0f);
		zoomValue = Mathf.Clamp(zoomValue, 1, 1.5f);

		SetMagnifierZoom( zoomValue);


		if(zoomValue >= DescriptionsManager.OffsetMin){
			offsetX = ((zoomValue - 1) * .14f) / 0.5f;
			offsetYMax = ((zoomValue - 1) * .1f) / 0.5f;
			offsetYMin = ((zoomValue - 1) * -0.13f) / 0.5f;
		}

	}

	Vector3 pos, toPos;
	Touch lastT;
	void Update()
	{
		if(magnify && descrMgr.currDescriptionUITex)
		{
			if(Input.touchCount  == 1 && Input.touches[0].phase == TouchPhase.Began){
				lastT = Input.touches[0];
			}
			if(Input.touchCount == 1)
			{
				if(Input.touches[0].phase == TouchPhase.Moved && zoomValue >= DescriptionsManager.OffsetMin ){
						float tempx = 0; 
						float tempy = 0; 
						//Debug.Log(Input.touches[0].position.x - lastT.position.x);
						if( Input.touches[0].position.x > lastT.position.x ){
							tempx = descrMgr.currDescriptionUITex.uvRect.x - 0.02f;
						}else if (Input.touches[0].position.x < lastT.position.x ){
							tempx = descrMgr.currDescriptionUITex.uvRect.x + 0.02f;
						}else{
							tempx = descrMgr.currDescriptionUITex.uvRect.x ;
						}
						if( Input.touches[0].position.y > lastT.position.y ){
							tempy = descrMgr.currDescriptionUITex.uvRect.y - 0.02f;
						}else if (Input.touches[0].position.y < lastT.position.y ){
							tempy = descrMgr.currDescriptionUITex.uvRect.y + 0.02f;
						}else{
							tempy = descrMgr.currDescriptionUITex.uvRect.y ;
						}
						lastT = Input.touches[0];
						if(tempy < offsetYMin){
							tempy = offsetYMin;
						}
						if(tempy > offsetYMax){
							tempy = offsetYMax;
						}
						if(tempx < -offsetX){
							tempx = -offsetX;
						}
						if(tempx > offsetX){
							tempx = offsetX;
						}
						if(descrMgr.currDescriptionUITex)descrMgr.currDescriptionUITex.uvRect = new Rect(tempx, tempy, 1, 1);
				}	
				if(  zoomValue < DescriptionsManager.OffsetMin  && Input.touches[0].phase == TouchPhase.Ended){
					descrMgr.currDescriptionUITex.uvRect = new Rect(0, 0, 1, 1);
				}
			}

		}

		if(Input.touchCount == 2 && magnifierEnabled)
		{
			t1 = Input.touches[0];
			t2 = Input.touches[1];

			if((t1.phase == TouchPhase.Ended && t2.phase == TouchPhase.Ended) && zoomValue < DescriptionsManager.OffsetMin) {
				descrMgr.currDescriptionUITex.uvRect = new Rect(0, 0, 1, 1);
			}
			if(t1.phase != TouchPhase.Moved && t2.phase != TouchPhase.Moved) {return;}

			ForceMagnifierActive();

			SmoothZoom(t1,t2);

		}
	}
}
