﻿using UnityEngine;
using System.Collections;

public class LoadingScript : MonoBehaviour
{
	AsyncOperation levelAsync;
	bool showARProgressBar;
	[SerializeField] GameObject loadingBar;
	[SerializeField] UITexture helpTex;
	[SerializeField] Texture2D vrHelpTex;
	static byte sceneID;
	
	void Start ()
	{
		if(sceneID == AppLibrary.SCENE_VERSO_360) helpTex.mainTexture = vrHelpTex;
		LoadScene();
	}

	public static void SetLevelToLoad(byte pSceneID)
	{
		sceneID = pSceneID;
	}

	void LoadScene()
	{
		levelAsync = Application.LoadLevelAsync(sceneID);
		showARProgressBar = true;
	}
	                                  
	void Update ()
	{
		if(!showARProgressBar) { return; }

		loadingBar.GetComponent<UITexture>().fillAmount = levelAsync.progress;
		
		if(levelAsync.progress > .85f)
		{
			loadingBar.GetComponent<UITexture>().fillAmount = 1;
			levelAsync.allowSceneActivation = true;
		}
	}
}
