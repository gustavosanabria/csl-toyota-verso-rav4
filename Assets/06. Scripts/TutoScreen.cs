﻿using UnityEngine;
using System.Collections;

public class TutoScreen : MonoBehaviour
{
	[SerializeField] UISpriteAnimation flashTextureAnim;
	[SerializeField] UIScrollView buttonsScroll;
	[SerializeField] float scrollAmmount;

	public void ClickDownArrow()
	{
		Scroll(-scrollAmmount);
	}

	public void ClickUpArrow()
	{
		Scroll(scrollAmmount);
	}

	void Scroll(float v)
	{
		buttonsScroll.Scroll(v);
		buttonsScroll.InvalidateBounds();
	}

	public void ClickTutBtn1()
	{
		print("TUT BTN 1");
	}

	public void ClickTutBtn2()
	{
		print("TUT BTN 2");
	}

	public void ClickTutBtn3()
	{
		print("TUT BTN 3");
	}

	public void ClickTutBtn4()
	{
		print("TUT BTN 4");
	}

	public void ClickTutBtn5()
	{
		print("TUT BTN 5");
	}

	public void ClickTutBtn6()
	{
		print("TUT BTN 6");
	}

	public void ClickTutBtn7()
	{
		print("TUT BTN 7");
	}

	public void ClickTutBtn8()
	{
		print("TUT BTN 8");
	}

	public void ClickTutBtn9()
	{
		print("TUT BTN 9");
	}

	public void ClickTutBtn10()
	{
		print("TUT BTN 10");
	}

	public void ClickTutBtn11()
	{
		print("TUT BTN 11");
	}
}
