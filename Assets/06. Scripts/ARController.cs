﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System.IO;

public class ARController : MonoBehaviour {
    private DataSet mDataset = null;
    private ObjectTracker tracker = null;
	public bool _loaded = false;

    void Update()
    {
        if (VuforiaRuntimeUtilities.IsVuforiaEnabled() && !_loaded)
        {
            if (mDataset == null)
            {
                tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
                mDataset = tracker.CreateDataSet();
            }

            //string filename = Path.GetFileName(tr.Path);
            //string externalPath = Application.persistentDataPath + "/Library/" + filename;
			string externalPath = Application.persistentDataPath + "/files/CSLVersoDB.xml";
            if (!mDataset.Load(externalPath, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
                Debug.Log("Couldn't load tracking info");

            tracker.ActivateDataSet(mDataset);

            _loaded = true;
        }
    }
}