﻿using UnityEngine;
using System.Collections;

public class TrackeabeManager : MonoBehaviour
{
	public static TrackeabeManager instance{ get; private set;}

	[SerializeField] GameObject[] steeringWheelmarkersGO;
	byte steeringWheelmarkersCount;

	[SerializeField] GameObject[] rav4SWMarkersGO;
	byte rav4SWMarkersCount;
	
	[SerializeField] GameObject[] rav4ACMarkersGO;
	byte rav4ACMarkersCount;

	[SerializeField] GameObject[] rav4TachometerGO;
	byte rav4TachometerMarkersCount;


	private TrackeabeManager() { instance = this; }

	public static TrackeabeManager GetInstance() { return instance; }

	void Awake()
	{
		steeringWheelmarkersCount = (byte)steeringWheelmarkersGO.Length;
		rav4SWMarkersCount = (byte)rav4SWMarkersGO.Length;
		rav4ACMarkersCount = (byte)rav4ACMarkersGO.Length;
		rav4TachometerMarkersCount = (byte)rav4TachometerGO.Length;
	}

	public void ActivateMarkers(byte markerID)
	{
		switch(markerID)
		{
			case AppLibrary.VERSO_MARKER_STEERINGWHEEL: ActivateAllSteeringWheelMarkers(); break;
			case AppLibrary.RAV4_MARKER_SW: ActivateAllRav4SWMarkers(); break;
			case AppLibrary.RAV4_MARKER_AC: ActivateAllRav4ACMarkers(); break;
			case AppLibrary.RAV4_MARKER_TACHOMETER:  ActivateAllRav4ATachometerMarkers(); break;
		}
	}

	public void DeactivateAlternateMarkers(byte markerID, GameObject workingMarker)
	{
		switch(markerID)
		{
			case AppLibrary.VERSO_MARKER_STEERINGWHEEL: DeactivateAlternateSteeringWheelMarker(workingMarker); break;
			case AppLibrary.RAV4_MARKER_SW: DeactivateAlternateRav4SWMarker(workingMarker); break;
			case AppLibrary.RAV4_MARKER_AC: DeactivateAlternateRav4ACMarker(workingMarker); break;
			case AppLibrary.RAV4_MARKER_TACHOMETER: DeactivateAlternateRav4TachometerMarker(workingMarker); break;
		}
	}

	void ActivateAllSteeringWheelMarkers()
	{
		print ("ACTIVATING ALL SW MARKERS");

		for (byte i = 0; i < steeringWheelmarkersCount; i++) steeringWheelmarkersGO[i].SetActive(true);
	}

	void DeactivateAlternateSteeringWheelMarker(GameObject workingMarker)
	{
		print ("DEACTIVATING ADDITIONAL SW MARKERS");

		for (byte i = 0; i < steeringWheelmarkersCount; i++)
		{
			if(steeringWheelmarkersGO[i] != workingMarker) steeringWheelmarkersGO[i].SetActive(false);
		}
	}

	void ActivateAllRav4SWMarkers()
	{
		print ("ACTIVATING ALL SW MARKERS");
		
		for (byte i = 0; i < rav4SWMarkersCount; i++) rav4SWMarkersGO[i].SetActive(true);
	}
	
	void DeactivateAlternateRav4SWMarker(GameObject workingMarker)
	{
		print ("DEACTIVATING ADDITIONAL SW MARKERS");
		
		for (byte i = 0; i < rav4SWMarkersCount; i++)
		{
			if(rav4SWMarkersGO[i] != workingMarker) rav4SWMarkersGO[i].SetActive(false);
		}
	}

	void ActivateAllRav4ACMarkers()
	{
		print ("ACTIVATING ALL AC MARKERS");
		
		for (byte i = 0; i < rav4ACMarkersCount; i++) rav4ACMarkersGO[i].SetActive(true);
	}

	void DeactivateAlternateRav4ACMarker(GameObject workingMarker)
	{
		print ("DEACTIVATING ADDITIONAL AC MARKERS");
		
		for (byte i = 0; i < rav4ACMarkersCount; i++)
		{
			if(rav4ACMarkersGO[i] != workingMarker) rav4ACMarkersGO[i].SetActive(false);
		}
	}

	void ActivateAllRav4ATachometerMarkers()
	{
		print ("ACTIVATING ALL TACH MARKERS");
		
		for (byte i = 0; i < rav4TachometerMarkersCount; i++) rav4TachometerGO[i].SetActive(true);
	}
	
	void DeactivateAlternateRav4TachometerMarker(GameObject workingMarker)
	{
		print ("DEACTIVATING ADDITIONAL TACH MARKERS");
		
		for (byte i = 0; i < rav4TachometerMarkersCount; i++)
		{
			if(rav4TachometerGO[i] != workingMarker) rav4TachometerGO[i].SetActive(false);
		}
	}
}
