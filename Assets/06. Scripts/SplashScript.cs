﻿using UnityEngine;
using System.Collections;

public class SplashScript : MonoBehaviour
{

	void Awake ()
    {
		print("AppType = " + AppLibrary.AppTYPE);
		

#if UNITY_ANDROID || UNITY_IOS

		Screen.sleepTimeout = SleepTimeout.NeverSleep;

		//LoadHomeScreen();
		//mediaCtrl.OnEnd += LoadHomeScreen; 	//disabled  28-01-2016

		//Handheld.PlayFullScreenMovie("VersoSplashScreen2.mp4", Color.black, FullScreenMovieControlMode.Hidden);
		
		//StartCoroutine(PlaySplashVideo());
		
		//ChangeScene();
#endif

#if UNITY_EDITOR
		//LoadHomeScreen();
#endif
	}
	
    void Update()
	{
		if(Input.touchCount>0)
		{
			ChangeScene();
			return;
		}

		//if(mediaCtrl.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.END) ChangeScene();
    }


	/*IEnumerator PlaySplashVideo()
	{
		Handheld.PlayFullScreenMovie("VersoSplashScreen2.mp4", Color.black, FullScreenMovieControlMode.Hidden);

		yield return new WaitForSeconds(5);

		LoadHomeScreen();
	}*/

    void LoadHomeScreen()
    {
        //print("DONE");
    
        Invoke("ChangeScene", 1);
    }

    void ChangeScene()
    {
        //Application.LoadLevel(1);
		MySceneManager.GoToHomeScreen(true);
    }
}
