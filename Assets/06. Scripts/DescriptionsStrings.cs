﻿using UnityEngine;
using System.Collections;

public class DescriptionsStrings : MonoBehaviour
{

    public string
                    versoPrecollision,
                    versoLights,
                    versoStabilityControl,
                    versoParkingAssist,
                    versoMirror,
                    versoVoiceCommand,
                    versoMode,
                    versoPhone,
                    versoDispTrip,
                    versoSpeedLimit,
                    versoSpeedRegulator,
                    versoLeftTachometer,
                    versoRightTachometer,
                    versoCenterScreen,
                    versoGPSLeftWheel,
                    versoGPSRightWheel,
                    versoGPSLeftMenu,
                    versoGPSRightMenu,
                    versoACLeftMenu,
                    versoACCenterMenu,
                    versoACRightMenu,
                    versoAirbag,
                    versoWindow,
                    versoPower,
					versoA_OFF;
}
