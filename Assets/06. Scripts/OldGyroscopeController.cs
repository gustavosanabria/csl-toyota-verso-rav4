// ***********************************************************
// Written by Heyworks Unity Studio http://unity.heyworks.com/
// ***********************************************************
using UnityEngine;

/// <summary>
/// Gyroscope controller that works with any device orientation.
/// </summary>
public class OldGyroscopeController : MonoBehaviour
{
    private readonly Quaternion baseIdentity = Quaternion.Euler(90, 0, 0);
    private Quaternion referanceRotation = Quaternion.identity;

    private Quaternion cameraBase = Quaternion.identity;
    private Quaternion calibration = Quaternion.identity;
    private Quaternion baseOrientation = Quaternion.Euler(90, 0, 0);
    private Quaternion baseOrientationRotationFix = Quaternion.identity;
	
    public bool gyroEnble = true;
    public float minSwipeDistY;
    public float minSwipeDistX;
    
    Vector2 startPos;
    float swipeDistVertical, swipeTime = 1;
    bool canCountUp = false;


    private Quaternion originalRotation;

#if UNITY_ANDROID
    private float sensitivityX = 0.2F;	//original: .75f
	private float sensitivityY = 0.2F; //original: .75f
#else
	private float sensitivityX = 0.5F;
	private float sensitivityY = 0.5F;
#endif

    private float minimumX = -360F;
    private float maximumX = 360F;

    private float minimumY = -60F;
    private float maximumY = 60F;

    private float rotationY = 0F;
    private float rotationX = 0F;

	/// <ADD>
	[SerializeField] float maxUPRot, maxDownRot;
	Vector3 headRot,headRot2,newVector;
	float xRot,xRot2,xLimit,newX;
	/// </END ADD>


	/// <ADD>
	/*void Awake()
	{
		headRot2 = Cardboard.SDK.HeadPose.Orientation.eulerAngles;
		
		xRot2 = headRot2.x;
		
		newX = Mathf.Abs(maxDownRot-xRot2) < Mathf.Abs(maxUPRot-xRot2) ? maxDownRot * .9f : maxUPRot * .9f;
		
		if((xRot2 > maxDownRot && xRot2 < maxUPRot) || (xRot2 < maxUPRot && xRot2 > maxDownRot))
		{
			transform.rotation = Quaternion.Euler(newX, headRot2.y, headRot2.z);
		}
	}*/
	/// <END ADD>

    protected void Start()
    {
		print(SystemInfo.graphicsDeviceVersion);

        AttachGyro();
    }

    protected void Update()
    {
//#if !UNITY_EDITOR
		/// <ADD>
		//headRot = Cardboard.SDK.HeadPose.Orientation.eulerAngles;
		//xRot = headRot.x;
		/// <END ADD>

		if (Input.touchCount == 1)
		{
			Touch touch = Input.touches [0];
			if (touch.phase == TouchPhase.Moved) {
				if (gyroEnble)
				{
					Quaternion temp1 = cameraBase  * (ConvertRotation (referanceRotation * Input.gyro.attitude) * GetRotFix ());
					temp1 = ClampRotationAroundXAxis(temp1);
					transform.localRotation = Quaternion.Slerp (transform.localRotation, Quaternion.Euler (temp1.eulerAngles.x, transform.eulerAngles.y - touch.deltaPosition.x, temp1.eulerAngles.z), sensitivityX);
					transform.eulerAngles = new Vector3 (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);

					CalibrateGyro();
				} else {
					rotationX += touch.deltaPosition.x * sensitivityX;
					rotationY += touch.deltaPosition.y * sensitivityY;
					
					rotationX = ClampAngle (rotationX, minimumX, maximumX);
					rotationY = ClampAngle (rotationY, minimumY, maximumY);
					
					Quaternion xQuaternion = Quaternion.AngleAxis (rotationX, Vector3.up);
					Quaternion yQuaternion = Quaternion.AngleAxis (rotationY, -Vector3.right);
					
					transform.localRotation = ConvertRotation (referanceRotation * originalRotation * xQuaternion * yQuaternion);
					transform.localRotation = ClampRotationAroundXAxis(transform.localRotation);
				}
			}
		}else if(Input.touchCount == 2)
		{	
			if(Input.touches[0].phase != TouchPhase.Moved && Input.touches[1].phase != TouchPhase.Moved) { return; }
			//SmoothZoom(Vector3.Distance(Input.touches[0].position, Input.touches[1].position));
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			
			SmoothZoom(touchZero, touchOne);
		}else if(gyroEnble){
			transform.rotation = cameraBase * (ConvertRotation (referanceRotation * Input.gyro.attitude) * GetRotFix ());
			transform.rotation = ClampRotationAroundXAxis(transform.rotation);
		}
		/// <ADD>
		/*if(Input.touchCount == 0)
		{
			if((xRot < maxDownRot && xRot > 0) || (xRot > maxUPRot && xRot < 360.1f)) 
			{
				gyroEnble = true;
			}
			else 
			{
				xLimit = GetLimit(xRot);
				newVector = new Vector3(xLimit, headRot.y, headRot.z);
				headRot = newVector;
			}
		}

		if (gyroEnble && Input.touchCount == 0)
		{
			transform.rotation = Quaternion.Euler(headRot);
			//transform.rotation = cameraBase * (ConvertRotation (referanceRotation * Input.gyro.attitude) * GetRotFix ());
		}*/
		/// <END ADD>
        countSwipeTimeUp();
//#endif
    }

	[SerializeField] float upX, downX;
	Quaternion ClampRotationAroundXAxis(Quaternion q)
	{
		q.x /= q.w;
		q.y /= q.w;
		q.z = 0;
		q.w = 1.0f;
		
		float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);
		
		//angleX = Mathf.Clamp (angleX, -20, 21);
		angleX = Mathf.Clamp (angleX, upX, downX);
		
		q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);
		
		return q;
	}
	/// <ADD>
	float GetLimit(float currentX)
	{
		return Mathf.Abs(maxDownRot-currentX) < Mathf.Abs(maxUPRot-currentX) ? maxDownRot : maxUPRot;
	}
	/// <END ADD>
	/*void SmoothZoom(float distance){
		if(distance > 600) distance = 600;
		if(distance < 200) distance = 200;
		distance -= 200;
		float value = ((1 - (distance/400)) * 32) + 28;
		value = Mathf.Clamp(value, 28, 60);
		Camera.main.fieldOfView = value;
	}*/

	void SmoothZoom(Touch touchZero, Touch touchOne)
	{
		float perspectiveZoomSpeed= 0.1f;
		// The rate of change of the field of view in perspective mode.
		// Find the position in the previous frame of each touch.
		Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
		Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
		
		// Find the magnitude of the vector (the distance) between the touches in each frame.
		float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
		float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
		
		// Find the difference in the distances between each frame.
		float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
		
		// fov between 35 and 59
		float fov = Camera.main.fieldOfView;
		fov += deltaMagnitudeDiff * perspectiveZoomSpeed;
		//fov = Mathf.Clamp(fov, 35.0f, 75.0f);
		fov = Mathf.Clamp(fov, 35f, 59f);
		
		Camera.main.fieldOfView = fov;
	}
   
	/// <summary>
    /// Attaches gyro controller to the transform.
    /// </summary>
    private void AttachGyro()
    {
        Input.gyro.enabled = true;
        gyroEnble = Input.gyro.enabled;

		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;

        if (gyroEnble)
        {
			CalibrateGyro();
        }

		originalRotation = transform.localRotation;
    }

    /// <summary>
    /// Detaches gyro controller from the transform
    /// </summary>
    private void DetachGyro()
    {
        Input.gyro.enabled = false;
    }

	private void CalibrateGyro()
	{
		ResetBaseOrientation();
		UpdateCalibration(true);
		UpdateCameraBaseRotation(true);
		RecalculateReferenceRotation();
	}

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    /// <summary>
    /// Update the gyro calibration.
    /// </summary>
    private void UpdateCalibration(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            var fw = (Input.gyro.attitude) * (-Vector3.forward);
            fw.z = 0;
            if (fw == Vector3.zero)
            {
                calibration = Quaternion.identity;
            }
            else
            {
                calibration = (Quaternion.FromToRotation(baseOrientationRotationFix * Vector3.up, fw));
            }
        }
        else
        {
            calibration = Input.gyro.attitude;
        }
    }

    /// <summary>
    /// Update the camera base rotation.
    /// </summary>
    /// <param name='onlyHorizontal'>
    /// Only y rotation.
    /// </param>
    private void UpdateCameraBaseRotation(bool onlyHorizontal)
    {
        if (onlyHorizontal)
        {
            var fw = transform.forward;
            fw.y = 0;
            if (fw == Vector3.zero)
            {
                cameraBase = Quaternion.identity;
            }
            else
            {
                cameraBase = Quaternion.FromToRotation(Vector3.forward, fw);
            }
        }
        else
        {
            cameraBase = transform.rotation;
        }
    }

    /// <summary>
    /// Converts the rotation from right handed to left handed.
    /// </summary>
    /// <returns>
    /// The result rotation.
    /// </returns>
    /// <param name='q'>
    /// The rotation to convert.
    /// </param>
    private Quaternion ConvertRotation(Quaternion q)
    {
		return new Quaternion(q.x, q.y, -q.z, -q.w);
    }

    /// <summary>
    /// Gets the rot fix for different orientations.
    /// </summary>
    /// <returns>
    /// The rot fix.
    /// </returns>
    private Quaternion GetRotFix()
    {
#if UNITY_3_5
		if (Screen.orientation == ScreenOrientation.Portrait)
			return Quaternion.identity;
		
		if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.Landscape)
			return landscapeLeft;
				
		if (Screen.orientation == ScreenOrientation.LandscapeRight)
			return landscapeRight;
				
		if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
			return upsideDown;
		return Quaternion.identity;
#else
        return Quaternion.identity;
#endif
    }

    /// <summary>
    /// Recalculates reference system.
    /// </summary>
    private void ResetBaseOrientation()
    {
        baseOrientationRotationFix = GetRotFix();
        baseOrientation = baseOrientationRotationFix * baseIdentity;
    }

    /// <summary>
    /// Recalculates reference rotation.
    /// </summary>
    private void RecalculateReferenceRotation()
    {
        referanceRotation = Quaternion.Inverse(baseOrientation) * Quaternion.Inverse(calibration);
    }


    void countSwipeTimeUp()
    {
        if (canCountUp)
        {
            swipeTime += Time.deltaTime;
        }
    }
}
