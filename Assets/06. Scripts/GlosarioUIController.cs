﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GlosarioUIController : MonoBehaviour {
    private GameObject _panelIdioma;
    private GameObject _panelGlosario;
    
    private GameObject _body;

    private GameObject _panelDefinicion;

    public Text txtSearch;

	[SerializeField] Transform buttonsParent;
	[SerializeField] UILabel label;
	[SerializeField] UIScrollBar scrollBar;

    void Start()
    {
        //_panelIdioma   = transform.FindChild("panel_idioma").gameObject;
        //_panelGlosario = transform.FindChild("panel_glosario").gameObject;
        //_panelDefinicion = transform.FindChild("panel_definicion").gameObject;
		
        //_body = Instantiate(transform.FindChild("panel_glosario").FindChild("body").gameObject);
    }

    public void Load(string code)
    {
        GlosarioController.Instance.LoadGlossary(code);

       // _panelIdioma.SetActive(false);
        //_panelGlosario.SetActive(true);
		List<string> w = GlosarioController.Instance.GetWords();
		/*for (int i = 0; i < w.Count; i++)
		{
			print(w[i]);
		}*/
        StartCoroutine(LoadWords(w));
    }

    public IEnumerator LoadWords(List<string> words)
    {
        if (words.Count == 0)
        { words.Add("Aucun résultat trouvé"); }

        buttonsParent.GetComponent<UIGrid>().maxPerLine = words.Count > 1 ? 2 : 1;

        foreach (string word in words)
        {
            StartCoroutine(LoadRow(word));
            yield return null;
        }

        yield return true;
    }

    public void OnClick_Row(string word)
    {
        string definition = GlosarioController.Instance.GetDefinition(word);
        string defTitle = GlosarioController.Instance.GetDefinitionTitle(word);
		string defTarget = GlosarioController.Instance.GetDefinitionTarget(word);	//MOD GLOSSARY V.2
		string defTut = GlosarioController.Instance.GetDefinitionTut(word);			//MOD GLOSSARY V.2

		//GetComponent<GlossaryMgr>().GetDefinition(defTitle, definition);
		GetComponent<GlossaryMgr>().GetDefinition(defTitle, definition, defTarget, defTut);	//MOD GLOSSARY V.2
		
		GlossaryMgr.LASTDEFWORD = word;	//MOD GLOSSARY V.2

        //-_panelDefinicion.SetActive(true);
        //-_panelDefinicion.transform.FindChild("Text").GetComponent<Text>().text = definition;
    }
    int btnN;

    public void Search()
    {
        /*string txt = txtSearch.text;
        if (string.IsNullOrEmpty(txt)) return;

        ClearList();
        StartCoroutine(LoadWords(GlosarioController.Instance.Search(txt)));*/

        //string txt = txtSearch.text;
		string txt = label.text;
        ClearList();
        btnN = 0;
        if (string.IsNullOrEmpty(txt))
        {
            StartCoroutine(LoadWords(GlosarioController.Instance.GetWords()));
        }
        else
        {
            StartCoroutine(LoadWords(GlosarioController.Instance.Search(txt)));
        }
    }

    private IEnumerator LoadRow(string word)
    {
        btnN++;
        //-GameObject row = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Glosario/row"));
        //-row.transform.GetComponentInChildren<Text>().text = word;
		GameObject row = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Glosario/GlossaryBtn2"));
        row.name = btnN.ToString() + " " + row.name;
        //row.transform.GetComponent<UISprite>().spriteName = word;
        //row.GetComponent<UISprite> ().MakePixelPerfect ();
        row.GetComponent<UILabel>().text = word;

        row.transform.parent = buttonsParent;
		row.transform.localScale = Vector3.one;

		if (word != "Aucun résultat trouvé")
        {
			UIButton btn = row.GetComponent<UIButton> ();
			EventDelegate del = new EventDelegate (this, "OnClick_Row");
			del.parameters [0].value = word;
			EventDelegate.Set (btn.onClick, del);

			if (word == GlossaryMgr.LASTDEFWORD) btn.SendMessage("OnClick"); //MOD GLOSSARY V.2

			//row.GetComponent<Button>().onClick.AddListener(() => { OnClick_Row(word); });
			//row.transform.parent = _panelGlosario.transform.FindChild("body");
			//-row.transform.SetParent(_panelGlosario.transform.FindChild("body")); 
		}

        buttonsParent.GetComponent<UIGrid>().Reposition();
        scrollBar.value = 0;
        scrollBar.barSize = 1;
        scrollBar.ForceUpdate();
        yield return null;
    }

    private void ClearList()
    {
		DestroyAllButtons ();

		return;

        GameObject.Destroy(_panelGlosario.transform.FindChild("body").gameObject);
        GameObject body = Instantiate(_body);
        body.name = "body";
        //body.transform.parent = _panelGlosario.transform;
        body.transform.SetParent(_panelGlosario.transform);
        body.transform.SetAsFirstSibling();
        (body.transform as RectTransform).offsetMax = Vector2.zero;
        (body.transform as RectTransform).offsetMin = Vector2.zero;
    }

	void DestroyAllButtons()
	{
		for (int i = 0; i < buttonsParent.childCount; i++)
		{
			Destroy(buttonsParent.GetChild(i).gameObject);
		}
		buttonsParent.GetComponent<UIGrid>().Reposition();
        scrollBar.ForceUpdate();
		scrollBar.value = 0;
	}
}
