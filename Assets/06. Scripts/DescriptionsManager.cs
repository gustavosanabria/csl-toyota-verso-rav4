﻿using UnityEngine;
using System.Collections;

public partial class DescriptionsManager : MonoBehaviour
{
    //[SerializeField] GameObject[] lightIconsDescriptions;
    //byte lightIconsDescriptionsCount;
	float lightIconsDescriptionsTweenSpeed = .3f;
	[SerializeField] UICamera descriptionsUICam;
	[SerializeField] GameObject loadingTex;
	[SerializeField] GameObject descriptionsBackground;
	[SerializeField] UITexture descriptionTexture;
	public UITexture currDescriptionUITex {get; private set;}
	public float currDescrOriginalWidth{get; private set;}
	public float currDescrOriginalHeight{get; private set;}
	GameObject extendedDescriptionContainer;
	[SerializeField] float extendedDescriptionScrollValue;
	[SerializeField] UIPanel extendedDescriptionScrollPanel, airbagDescrScrollPanel, acCenterScrollPanel, acLeftScrollPanel, acRightScrollPanel;

    Vector3 extendedDescriptionOriginalPanelPos, airbagDescrOriginalPanelPos, acCenterDescrOriginalPanelPos, acLeftDescrOriginalPanelPos, acRightDescrOriginalPanelPos;
    Vector2 extendedDescriptionOriginalClip, airbagDescrOriginalClip, acCenterDescrOriginalClip, acLeftDescrOriginalClip, acRightDescrOriginalClip;

	Quaternion zAxis180Rotation = new Quaternion(0,0,180,0);

	[SerializeField] UICamera guiCameraUIScript;
	public delegate void onVideoEndDel();
	public event onVideoEndDel evs;

	void Start ()
	{
		evs += ShowTargets;
        //lightIconsDescriptionsCount = (byte)lightIconsDescriptions.Length;

		extendedDescriptionOriginalPanelPos = extendedDescriptionScrollPanel.transform.localPosition;
		extendedDescriptionOriginalClip = extendedDescriptionScrollPanel.clipOffset;

		airbagDescrOriginalPanelPos = airbagDescrScrollPanel.transform.localPosition;
		airbagDescrOriginalClip = airbagDescrScrollPanel.clipOffset;

        acCenterDescrOriginalPanelPos = acCenterScrollPanel.transform.localPosition;
        acCenterDescrOriginalClip = acCenterScrollPanel.clipOffset;

        acLeftDescrOriginalPanelPos = acLeftScrollPanel.transform.localPosition;
        acLeftDescrOriginalClip = acLeftScrollPanel.clipOffset;

        acRightDescrOriginalPanelPos = acRightScrollPanel.transform.localPosition;
        acRightDescrOriginalClip = acRightScrollPanel.clipOffset;

        //SetLightIconsTweenSpeed();
		#if UNITY_IOS
			Destroy(descriptionsBackground.GetComponentInChildren<UIPanel>());
		#endif
        SetDictionary();

	}

    //void SetLightIconsTweenSpeed()
    //{
    //    for (byte i = 0; i < lightIconsDescriptionsCount; i++)
    //    {
    //        lightIconsDescriptions[i].GetComponent<TweenScale>().duration = lightIconsDescriptionsTweenSpeed;
    //        lightIconsDescriptions[i].GetComponent<TweenPosition>().duration = lightIconsDescriptionsTweenSpeed;
    //    }
    //}

	public void EnableClick(bool b)
	{
		descriptionsUICam.enabled= b;
	}

	public void ShowLoading()
	{
		loadingTex.SetActive(true);
	}

    public void GetVersoDescriptionByString(string descrStr)
    {
        string versoPath = "Augmented Reality/Button description Screen/Verso/SeparateDescriptions";

		descriptionTexture.gameObject.SetActive(false);

        StartCoroutine(GetTexture(descrStr, versoPath));
    }

	public void SpeedRegulatorDescription(GameObject descrContainer)
	{
		ShowExtendedDescription(descrContainer);
	}

	public void AirbagDescription(GameObject descrContainer)
	{
		ShowAirbagExtendedDescription(descrContainer);
	}

    public void AcCenterDescription(GameObject descrContainer)
    {
        ShowAcCenterExtendedDescription(descrContainer);
    }

    public void AcLeftDescription(GameObject descrContainer)
    {
        ShowAcLeftExtendedDescription(descrContainer);
    }

    public void AcRightDescription(GameObject descrContainer)
    {
        ShowAcRightExtendedDescription(descrContainer);
    }

	public void ShowTireKitVideo()
	{
		AppLibrary.CURRENTVIDEONAME = AppLibrary.VIDEO_VR;
		//Application.LoadLevelAdditive(AppLibrary.SCENE_AR_HELPVIDEO);
		Application.LoadLevel(AppLibrary.SCENE_AR_HELPVIDEO);
		//DisableTargets();
	}

	void DisableTargets()
	{
		if(guiCameraUIScript) guiCameraUIScript.enabled = false;
		//GetComponent<TargetManager>().EnableTargets(false);
		GetComponent<TargetManager>().ShowTargets(false);
	}

	public void ShowTargets()
	{
		Resources.UnloadUnusedAssets();
		System.GC.Collect();

		if(guiCameraUIScript) guiCameraUIScript.enabled = true;
		GetComponent<TargetManager>().EnableTargets(true);
		GetComponent<TargetManager>().ShowTargets(true);
	}

    void ResetAndPlayTween(GameObject go)
	{
		go.GetComponent<TweenScale>().PlayForward();
		go.GetComponent<TweenScale>().ResetToBeginning();
		go.GetComponent<TweenScale>().PlayForward();
	}

	public void CloseGPSConfirmationWindow()
	{
		GetComponent<TargetManager>().EnableTargets (true);
	}

	public void CloseTireKitPopUpWindow()
	{
		GetComponent<TargetManager>().EnableTargets (true);
	}

	public static float OffsetMin = 1;
	void ShowDescription()
	{

        OffsetMin = 1;
		//ResetAndPlayTween(descriptionTexture.gameObject);
		//ResetAndPlayTween(descriptionsBackground);
		currDescriptionUITex = descriptionTexture;
		currDescrOriginalWidth = currDescriptionUITex.width;
		currDescrOriginalHeight= currDescriptionUITex.height;
	}

	void ShowExtendedDescription(GameObject container)
	{
		extendedDescriptionScrollPanel.gameObject.SetActive (true);
		extendedDescriptionScrollPanel.transform.localPosition = extendedDescriptionOriginalPanelPos;
		extendedDescriptionScrollPanel.clipOffset = extendedDescriptionOriginalClip;
		extendedDescriptionContainer = container;
		currDescriptionUITex = container.transform.GetChild(1).GetComponentInChildren<UITexture>();
		currDescrOriginalWidth = currDescriptionUITex.width;
		currDescrOriginalHeight= currDescriptionUITex.height;
		ResetAndPlayTween(extendedDescriptionContainer);
	}

	void ShowAirbagExtendedDescription(GameObject container)
	{
		airbagDescrScrollPanel.gameObject.SetActive (true);
		airbagDescrScrollPanel.transform.localPosition = airbagDescrOriginalPanelPos;
		airbagDescrScrollPanel.clipOffset = airbagDescrOriginalClip;
		extendedDescriptionContainer = container;
		currDescriptionUITex = container.transform.GetChild(1).GetComponentInChildren<UITexture>();
		currDescrOriginalWidth = currDescriptionUITex.width;
		currDescrOriginalHeight = currDescriptionUITex.height;
		ResetAndPlayTween(extendedDescriptionContainer);
	}

    void ShowAcCenterExtendedDescription(GameObject container)
    {
        acCenterScrollPanel.gameObject.SetActive(true);
        acCenterScrollPanel.transform.localPosition = acCenterDescrOriginalPanelPos;
        acCenterScrollPanel.clipOffset = acCenterDescrOriginalClip;
        extendedDescriptionContainer = container;
        currDescriptionUITex = container.transform.GetChild(1).GetComponentInChildren<UITexture>();
        currDescrOriginalWidth = currDescriptionUITex.width;
        currDescrOriginalHeight = currDescriptionUITex.height;
        ResetAndPlayTween(extendedDescriptionContainer);
    }

    void ShowAcLeftExtendedDescription(GameObject container)
    {
        acLeftScrollPanel.gameObject.SetActive(true);
        acLeftScrollPanel.transform.localPosition = acLeftDescrOriginalPanelPos;
        acLeftScrollPanel.clipOffset = acLeftDescrOriginalClip;
        extendedDescriptionContainer = container;
        currDescriptionUITex = container.transform.GetChild(1).GetComponentInChildren<UITexture>();
        currDescrOriginalWidth = currDescriptionUITex.width;
        currDescrOriginalHeight = currDescriptionUITex.height;
        ResetAndPlayTween(extendedDescriptionContainer);
    }

    void ShowAcRightExtendedDescription(GameObject container)
    {
        acRightScrollPanel.gameObject.SetActive(true);
        acRightScrollPanel.transform.localPosition = acRightDescrOriginalPanelPos;
        acRightScrollPanel.clipOffset = acRightDescrOriginalClip;
        extendedDescriptionContainer = container;
        currDescriptionUITex = container.transform.GetChild(1).GetComponentInChildren<UITexture>();
        currDescrOriginalWidth = currDescriptionUITex.width;
        currDescrOriginalHeight = currDescriptionUITex.height;
        ResetAndPlayTween(extendedDescriptionContainer);
    }

    public void CloseDescription()
	{
		if(currDescriptionUITex)
		{
			currDescriptionUITex.width = (int)currDescrOriginalWidth;
			currDescriptionUITex.height = (int)currDescrOriginalHeight;
			currDescriptionUITex.uvRect = new Rect(0,0,1,1);
			currDescriptionUITex = null;
		}
		//descriptionTexture.GetComponent<TweenScale>().PlayReverse();
		descriptionsBackground.GetComponent<TweenScale>().PlayReverse();
		GetComponent<TargetManager>().EnableTargets(true);
	}

	public void CloseExtendedDescription()
	{
		if(currDescriptionUITex)
		{
			currDescriptionUITex.width = (int)currDescrOriginalWidth;
			currDescriptionUITex.height = (int)currDescrOriginalHeight;
			currDescriptionUITex.uvRect = new Rect(0,0,1,1);
			currDescriptionUITex = null;
		}
		extendedDescriptionContainer.GetComponent<TweenScale>().PlayReverse();
		GetComponent<TargetManager>().EnableTargets(true);
		extendedDescriptionContainer.transform.FindChild ("Panel").gameObject.SetActive (false);
	}
	
	public void BtnDownExtendedDescription(UIScrollView uiSV)
	{
		ScrollDescription(uiSV, -extendedDescriptionScrollValue);
	}

	public void BtnUpExtendedDescription(UIScrollView uiSV)
	{
		ScrollDescription(uiSV, extendedDescriptionScrollValue);
	}

	void ScrollDescription(UIScrollView scrView, float v)
	{
		scrView.InvalidateBounds ();
		scrView.Scroll (v);
		scrView.InvalidateBounds ();
	}
}
