﻿using UnityEngine;
using System.Collections;

public class TargetManager : MonoBehaviour
{
	[SerializeField] GameObject[] targets;
	int targetsCount;
	[SerializeField] UICamera targetsCameraUIScript;
	[SerializeField] Sprite circleTargetHiLight;
	[SerializeField] Sprite rectTargetHiLight;
	[SerializeField] bool enableAnims;
	[SerializeField] GameObject magnifierGO;
	private bool _show = true;
	public GameObject Targets;

	[SerializeField] VRCameraClamper vrCameraClamper;

	void LateUpdate()
	{
		if (_show)
		{
			_show = false;
			if(Targets)Targets.SetActive(true);
			
			targets = GameObject.FindGameObjectsWithTag("Target");
			targetsCount = targets.Length;
			print(targetsCount);
			if (enableAnims) EnableTargetAnim();
			
			for (int i = 0; i < targetsCount; i++) { targets[i].GetComponent<Collider>().enabled = true; }

			if(GlossaryMgr.vrTargetsToShow.Count > 0)	//MOD GLOSSARY V.2
			{
				foreach(GameObject targetGO in targets)
				{	
					for (int i = 0; i < GlossaryMgr.vrTargetsToShow.Count; i++)
					{
						if(targetGO.name.Contains(GlossaryMgr.vrTargetsToShow[i]+"."))
						{
							targetGO.SetActive(true);
							break;
						}
						else targetGO.SetActive(false);
					}
				}
			}
		}
	}


	void EnableTargetAnim()
	{
		Animator an;
		float time;
		string name = "name";
		
		time = 0f;
		
		for (int i = 0; i < targetsCount; i++)
		{
			an = targets[i].GetComponent<Animator>();
			
			time += 5;
			
			if (an.GetCurrentAnimatorStateInfo(0).IsName("CibleRectAnim")) name = "CibleRectAnim";
			else if (an.GetCurrentAnimatorStateInfo(0).IsName("CibleRondeAnim")) name = "CibleRondeAnim";
			else if (an.GetCurrentAnimatorStateInfo(0).IsName("CibleRectLongAnim 1")) name = "CibleRectLongAnim 1";

			an.enabled = true;
			an.Play(name, 0, .045f * time);
		}
	}

	public void EnableTargets(bool b)
	{
		//magnifierGO.SetActive(!b);

		if(vrCameraClamper)
		{
			//vrCameraClamper.EnableSwipeNavigation(b);
			//vrCameraClamper.ActivateModeSwitchBtn(b);
		}

		if(GetComponent<MagnifierScript>())
		{
			GetComponent<MagnifierScript>().MagnifierActive(!b);
		}

		if(b){GetComponent<MagnifierScript>().DisableMagnifier();}

		//targetsColBlocker.enabled = !b;
		EnableTargetUICamera(b);

		for (int i = 0; i < targetsCount; i++)
		{
			targets[i].GetComponent<Animator>().enabled = b;
		}

		//for (byte i = 0; i < targetsCount; i++) targets[i].enabled = b;
	}

	public void ShowTargets(bool b)
	{
		for (int i = 0; i < targetsCount; i++)
		{
			targets[i].GetComponent<SpriteRenderer>().enabled = b;
		}
	}

	public void StopTargetAnim(GameObject go)
	{
		go.GetComponent<Animator>().enabled = false;
	}

	public void EnableTargetUICamera(bool b)
	{
		targetsCameraUIScript.enabled = b;
	}

	public void CircleTargetClick(GameObject targetGO)
	{
		StopTargetAnim(targetGO);
		targetGO.GetComponent<SpriteRenderer>().sprite = circleTargetHiLight;
		EnableTargets(false);
	}

	public void RectTargetClick(GameObject targetGO)
	{
		StopTargetAnim(targetGO);
		targetGO.GetComponent<SpriteRenderer>().sprite = rectTargetHiLight;
		EnableTargets(false);
	}

	public void RectLongTargetClick(GameObject targetGO)
	{
		StopTargetAnim(targetGO);
		//targetGO.GetComponent<SpriteRenderer>().sprite = rectTargetHiLight;
		EnableTargets(false);
	}

	public void TachometerTarget2Click()
	{
		GetComponent<LightIconsManager>().ShowIcons();
		EnableTargets(false);
	}

	public void VolumeControlTargetClick()
	{
		print("Volume Control");
	}
}
