﻿using UnityEngine;
using System.Collections;

public class TutorialsHandler : MonoBehaviour
{
	[SerializeField] Tutorial[] tutorials;
	byte tutorialsCount;

	void Awake()
	{
		tutorialsCount = (byte)tutorials.Length;
	}

	public Tutorial GetTutorial(byte pID)
	{
		print("Requested Tut ID: " + pID);
		for (byte c = 0; c < tutorialsCount; c++)
		{
			if(tutorials[c].id == pID)
			{
				return tutorials[c];
			}
		}

		Debug.LogError("ERROR 404: TUTORIAL NOT FOUND");

        return new Tutorial();
	}
}

[System.Serializable]
public struct Tutorial
{
	public byte id;
    //[SerializeField] Texture2D[] steps;
	[SerializeField] Vector3[] targetPos;
    //[SerializeField] AudioClip[] clips;

    [SerializeField] string[] steps_names;

    [SerializeField] string clips_prefix;
    [SerializeField] int clips_startIndex;
    [SerializeField] int clips_endIndex;

	[SerializeField] byte automaticIndex, slidingIndex, slideSteps;
	[SerializeField] string[] searchLabels;
	
    //public Texture2D[] GetSteps() {return steps;}
	public Vector3[] GetTargetPos() {return targetPos;}
    //public AudioClip[] GetAudioClips() {return clips;}

    public void GenerateAudioPaths()
    {
        if (audio_paths == null || audio_paths.Length == 0)
        {
            audio_paths = new string[clips_endIndex - clips_startIndex + 1];
            int pos = 0;
            for (int i = clips_startIndex; i < clips_endIndex + 1; i++)
            {
                audio_paths[pos] = string.Format("Audio/{0}{1}", clips_prefix, i);
                pos++;
            }
        }
    }

    public byte Count()
    {
        return (byte)steps_names.Length;
    }

    public Texture2D LoadTexture(int pos)
    {
        string tex_path = string.Format("Tutorials/{0}", steps_names[pos]);
        if (tex_path.EndsWith(".jpg")) // comentar eso si la textura se carga desde StreamingAssets
            tex_path = tex_path.Replace(".jpg", "");

        return Resources.Load(tex_path) as Texture2D;
    }

    public AudioClip LoadAudio(int pos)
    {
        GenerateAudioPaths();
        return Resources.Load(audio_paths[pos]) as AudioClip;
    }

	public byte GetAutomaticIndex() {return automaticIndex;}
	public byte GetSlidingIndex() {return slidingIndex;}
	public byte GetSlideSteps() {return slideSteps;}
	public string[] GetSearchLabels() {return searchLabels;}

    private string[] audio_paths;
}
