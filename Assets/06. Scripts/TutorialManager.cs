using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialManager : MonoBehaviour
{
	[SerializeField] UICamera mainMenuCam;

	[SerializeField] GameObject tutorialBackground;
	[SerializeField] GameObject tutoWarning;

	[SerializeField] GameObject phoneBtnGO;
	[SerializeField] Texture2D phoneBtnHL, phoneBtn;
	[SerializeField] GameObject gpsBtnGO;
	[SerializeField] Texture2D gpsBtnHL, gpsBtn;
	[SerializeField] GameObject applicationBtnGO;
	[SerializeField] Texture2D applicationBtnHL, applicationBtn;
	[SerializeField] GameObject radioBtnGO;
	[SerializeField] Texture2D radioBtnHL, radioBtn;

	[SerializeField] GameObject warningContainer;
	[SerializeField] GameObject categoriesContainer;
	[SerializeField] GameObject phoneBtnsContainer;

	[SerializeField] UITexture tutStepScreen;

    //[SerializeField] Texture2D[] currentTutSteps;
	[SerializeField] Vector3[] currentTutTargetPos;
    //[SerializeField] AudioClip[] currentTutClips;
	byte currentTutStepsCount, currentTutAutomaticStepIndex;
	sbyte currentTutStepIndex = -1;
	sbyte tutStepDir;

	GameObject previousScreen;
	[SerializeField] List<GameObject> screensList = new List<GameObject>();

	byte screensCount;
	[SerializeField] GameObject tutorialContainer;

	[SerializeField] Tutorial currentTut;

	[SerializeField] float targetSpeed;
	[SerializeField] Transform targetTrans;
	[SerializeField] Sprite targetHLTex;
	[SerializeField] Collider prevStepBtnCol;
	[SerializeField] Collider audioBtnCol;
	[SerializeField] GameObject menuBtn;
	
	[SerializeField] TutorialsHandler tutoHandler;
	[SerializeField] UIScrollView timeLineScroll;
	[SerializeField] byte slidingIndex, slideSteps;
	GameObject nextScreen;
	bool slidedForward, slidedBackwards;
	[SerializeField] Collider[] stepsCols;
	byte stepsColsCount;
	[SerializeField] AudioSource audioSrc;

	void Start()
	{
		screensCount = (byte)screensList.Count;

		stepsColsCount = (byte)stepsCols.Length;

		SaveOriginalParameters();
	}

	Vector3 originalTimeLineToPos, originalTimeLineMaskTOPos;

	void SaveOriginalParameters()
	{
		originalTimeLineToPos = timeLineTrans.localPosition;
		originalTimeLineMaskTOPos = timeLineMaskTrans.localPosition;
	}

	void LoadParameters()
	{
		timeLineTrans.localPosition = originalTimeLineToPos;
		timeLineToPos = originalTimeLineToPos;
		timeLineMaskTrans.localPosition = originalTimeLineMaskTOPos;
		timeLineMaskTOPos = originalTimeLineMaskTOPos;
		slidedForward = slidedBackwards = false;
	}

	bool deployTutMenu;
	TweenScale tutMenuBtnsTween;
	public void TutorialMenuBtnClick(GameObject buttonsContainer)
	{
		deployTutMenu = !deployTutMenu;
		AnimateReturnButton(false);

		tutMenuBtnsTween = buttonsContainer.GetComponent<TweenScale>();

		if(deployTutMenu) buttonsContainer.GetComponent<TweenScale>().PlayForward();
		else buttonsContainer.GetComponent<TweenScale>().PlayReverse();
	}
	
	public void OpenUrlBtn()
	{
		Application.OpenURL("http://www.matoyota.fr");
	}

	public void JumpToGlossary()
	{
		Application.LoadLevel(AppLibrary.SCENE_GLOSSARY);
	}

	public void SetTutorial(byte tutID)
	{
		AnimateReturnButton(false);
		LoadParameters();
		mainMenuCam.enabled = false;

		tutorialContainer.SetActive(true);
		currentTut = tutoHandler.GetTutorial(tutID);
        //currentTutSteps = currentTut.GetSteps();
		currentTutTargetPos = currentTut.GetTargetPos();
        //currentTutClips = currentTut.GetAudioClips();
        currentTut.GenerateAudioPaths();
        //currentTutStepsCount = (byte)currentTutSteps.Length;
        currentTutStepsCount = currentTut.Count();
		currentTutAutomaticStepIndex = currentTut.GetAutomaticIndex();
		slidingIndex = currentTut.GetSlidingIndex();
		slideSteps = currentTut.GetSlideSteps();
		currentTutStepIndex = -1;
		targetTrans.localPosition = currentTutTargetPos[0];
		tutStepDir = 1;
		toggleAudio = true;
		ChangeTutStep();
	}

	public void NextScreen(GameObject nextScr = null)
	{
		CancelInvoke("NextScreen");
		if(nextScr == null) {nextScr = nextScreen;}

		if(screensList.Count>0) {screensList[screensList.Count-1].SetActive(false);}
		else {tutoWarning.SetActive(false);}

		if(!screensList.Contains(nextScr)) {screensList.Add(nextScr);}

		nextScr.transform.localScale = Vector3.zero;
		nextScr.SetActive(true);
		nextScr.GetComponent<TweenScale>().ResetToBeginning();
		nextScr.GetComponent<TweenScale>().PlayForward();
	}

	public void BackToPreviousScreen()
	{
		if(screensList.Count<1) {CloseTutoWindow(); return;}

		screensList[screensList.Count-1].SetActive(false);
		screensList[screensList.Count-1].transform.localScale = Vector3.zero;
		screensList.RemoveAt(screensList.Count -1);

		if(screensList.Count<1) {CloseTutoWindow(); return;}
		
		previousScreen = screensList[screensList.Count-1];
		previousScreen.transform.localScale = Vector3.one;
		previousScreen.SetActive(true);
	}

	void CloseTutoWindow()
	{
		GetComponent<HomeScreenManager>().ToMainMenu();
		tutorialBackground.transform.localScale = tutorialBackground.transform.position = Vector3.zero;
	}

	public void CloseTutAndGoBackToShit()
	{
		mainMenuCam.enabled = true;
		tutorialContainer.SetActive(false);
	}

	public void CloseTutAndGoBackToPreviousScreen()
	{
		mainMenuCam.enabled = true;
		tutorialContainer.SetActive(false);
		BackToPreviousScreen();
	}
	
	public void EnableWarning()
	{
		tutoWarning.SetActive(true);
	}

	public void ShowTutorialCategories()
	{
		warningContainer.SetActive(false);
		categoriesContainer.SetActive(true);
	}

	void OpenCategory()
	{
		NextScreen();
	}

	public void PhoneBtnClick(GameObject category)
	{
		nextScreen = category;
		CancelInvoke("OpenCategory");
		Invoke("OpenCategory", .15f);
		if(deployTutMenu) {tutMenuBtnsTween.PlayReverse(); deployTutMenu = false;}
	}

	public void ResetPhoneBtn()
	{
		phoneBtnGO.GetComponent<TweenScale>().ResetToBeginning();
	}

	public void GPSBtnClick(GameObject category)
	{
		nextScreen = category;
		CancelInvoke("OpenCategory");
		Invoke("OpenCategory", .15f);
		if(deployTutMenu) {tutMenuBtnsTween.PlayReverse(); deployTutMenu = false;}
	}

	public void ResetGPSBtn()
	{
		gpsBtnGO.GetComponent<TweenScale>().ResetToBeginning();
	}
	
	public void ApplicationBtnClick(GameObject category)
	{
		nextScreen = category;
		CancelInvoke("OpenCategory");
		Invoke("OpenCategory", .15f);
		if(deployTutMenu) {tutMenuBtnsTween.PlayReverse(); deployTutMenu = false;}
	}

	public void ResetApplicationBtn()
	{
		applicationBtnGO.GetComponent<TweenScale>().ResetToBeginning();
	}

	public void RadioBtnClick(GameObject category)
	{
		nextScreen = category;
		CancelInvoke("OpenCategory");
		Invoke("OpenCategory", .15f);
		if(deployTutMenu) {tutMenuBtnsTween.PlayReverse(); deployTutMenu = false;}
	}

	public void ResetRadionBtn()
	{
		radioBtnGO.GetComponent<TweenScale>().ResetToBeginning();
	}
	
	public void ResetAllScreens()
	{
		for (byte i = 0; i < screensCount; i++)
		{
			screensList[i].transform.localScale = Vector3.zero;
			screensList[i].SetActive(false);
		}
	}

	public void GoToNextTutStep()
	{ 
		if(currentTutStepIndex == currentTutStepsCount-1) return;

		SlideTimeLineMask(timeLineMaskSlideValue);

		EnableTarget(false, false);
		CancelInvoke("ChangeTutStep");

		tutStepDir = 1;
		Invoke("ChangeTutStep", .3f);
	}

	public void GoToPrevTutStep()
	{ 
		if(currentTutStepIndex == 0) return;
		
		SlideTimeLineMask(-timeLineMaskSlideValue);
		
		EnableTarget(false, false);
		CancelInvoke("ChangeTutStep");

		tutStepDir = -1;
		Invoke("ChangeTutStep", .3f);
	}
	
	[SerializeField] float automaticStepDelay, scrollValue;
	void ChangeTutStep()
	{
		currentTutStepIndex += tutStepDir;
		//print("tutStep: " + currentTutStepIndex);
		EnableTarget(true, true);

		audioSrc.Stop();

        tutStepScreen.mainTexture = currentTut.LoadTexture(currentTutStepIndex);// currentTutSteps[currentTutStepIndex];;
		PlayAudio();
		
		if(currentTutStepIndex >= slidingIndex && tutStepDir > 0) {SlideTimeLine(slideSteps);}
		else if(currentTutStepIndex <= slidingIndex-1 && tutStepDir < 0) {SlideTimeLine(-slideSteps);}

		if(currentTutStepIndex == currentTutStepsCount-1)
		{
			targetTrans.gameObject.SetActive(false);
			if(!deployTutMenu) AnimateReturnButton(true);
		}
		else
		{
			targetTrans.gameObject.SetActive(true);
			AnimateReturnButton(false);
		}

		if(currentTutStepIndex == currentTutAutomaticStepIndex-1 && tutStepDir > 0)
		{
			CancelInvoke("ChangeTutStep");

			EnableTarget(true, false);
			Invoke("ChangeTutStep", automaticStepDelay);
		}
		else if(currentTutStepIndex == currentTutAutomaticStepIndex+1 && tutStepDir < 0)
		{
			CancelInvoke("ChangeTutStep");
		}
	}

	public void JumpToStep(sbyte pStepIndex)
	{
		if(pStepIndex > currentTutStepsCount) {return;}

		sbyte diff = 0;
		sbyte stepIndex = (sbyte)(pStepIndex-1);

		if(currentTutStepIndex == stepIndex) {return;}
		if(pStepIndex > currentTutStepIndex) {print("Cannot jump forward"); return;}

		diff = (sbyte)((stepIndex) - currentTutStepIndex);

		SlideTimeLineMask((float)(timeLineMaskSlideValue*(diff)));
		tutStepDir = diff;
		
		EnableTarget(false, false);
		CancelInvoke("ChangeTutStep");

		Invoke("ChangeTutStep", .3f);
	}

	[SerializeField] float timeLineMaskSlideValue, slideSpeed;
	[SerializeField] Transform timeLineTrans, timeLineMaskTrans;
	Vector3 timeLineMaskTOPos;
	bool updateTimeLine, slideTimeLine;

	void SlideTimeLineMask(float v)
	{
		timeLineMaskTOPos = timeLineMaskTrans.localPosition + new Vector3(v,0,0);
		timeLineToPos = timeLineTrans.localPosition;
		updateTimeLine = true;
	}

	bool toggleAudio = true;
	[SerializeField] UISprite audioBtnSpr;

	public void ToggleAudio()
	{
		toggleAudio = !toggleAudio;
		PlayAudio();
	}

	public void PlayAudio()
	{
		if(!toggleAudio) audioSrc.Stop();
		else 
		{
			audioSrc.Stop();
            audioSrc.clip = currentTut.LoadAudio(currentTutStepIndex); // currentTutClips[currentTutStepIndex];
			audioSrc.Play();
		}

		//audioBtnSpr.spriteName = toggleAudio ? "SoundButton" : "NosoundButton";
	}

	Vector3 timeLineToPos;
	void SlideTimeLine()
	{
		float v = (timeLineMaskSlideValue * ((currentTutStepsCount-2) - currentTutStepIndex)) * tutStepDir;
		timeLineToPos = timeLineTrans.localPosition + new Vector3 (-v,0,0);
	}

	void SlideTimeLine(float slideValue)
	{
		if(!slidedForward && slideValue<0) {timeLineToPos = timeLineTrans.localPosition;}

		if(!slidedForward && slideValue > 0)
		{
			slidedForward = true;
			float v = (timeLineMaskSlideValue * slideValue);
			timeLineToPos = timeLineTrans.localPosition + new Vector3 (-v,0,0);
		}
		else if (slidedForward && slideValue < 0)
		{
			slidedForward = false;
			float v = (timeLineMaskSlideValue * slideValue);
			timeLineToPos = timeLineTrans.localPosition + new Vector3 (-v,0,0);
		}
	}

	void Update()
	{
		audioBtnSpr.spriteName = toggleAudio ? "SoundButton" : "NosoundButton";

		if(updateTimeLine)
		{
			timeLineMaskTrans.localPosition = Vector3.Lerp(timeLineMaskTrans.localPosition, timeLineMaskTOPos, Time.deltaTime * slideSpeed);
		}

		if((currentTutStepIndex >= slidingIndex && tutStepDir > 0) || (currentTutStepIndex <= slidingIndex-1 && tutStepDir < 0))
		{
			timeLineTrans.localPosition = Vector3.Lerp(timeLineTrans.localPosition, timeLineToPos, Time.deltaTime * (slideSpeed * .75f));
		}

		if(currentTutStepIndex > -1)
		{
			targetTrans.localPosition = Vector3.Lerp(targetTrans.localPosition, currentTutTargetPos[currentTutStepIndex], Time.deltaTime * targetSpeed);
		}
	}

	void EnableTarget(bool anim, bool collider)
	{
		targetTrans.GetComponent<Animator>().enabled = anim;
		targetTrans.GetComponent<Collider>().enabled = collider;
		prevStepBtnCol.enabled = collider;
		audioBtnCol.enabled = collider;

		for (byte i = 0; i < stepsColsCount; i++) stepsCols[i].enabled = collider;
	}

	TweenScale twS;
	void AnimateReturnButton(bool b)
	{
		twS = menuBtn.GetComponent<TweenScale>();
		
		if (b)
		{
			twS.enabled = true;
			twS.style = UITweener.Style.Loop;
			twS.Play ();
		}
		else
		{
			twS.enabled = false;
			twS.ResetToBeginning();
			twS.style = UITweener.Style.Once;
			menuBtn.transform.localScale = twS.from;
		}
	}
	
	public void TargetClick()
	{
		targetTrans.GetComponent<Animator>().enabled = false;
		targetTrans.GetComponent<SpriteRenderer>().sprite = targetHLTex;
	}

    public void DestroyMe()
    {
        DestroyImmediate(tutorialBackground);
        DestroyImmediate(tutorialContainer);
		DestroyImmediate (tutoHandler.gameObject);
    }
}