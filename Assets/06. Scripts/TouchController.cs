﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour
{

    private Quaternion referanceRotation = Quaternion.identity;
    private Quaternion originalRotation;

    private float sensitivityX = 0.75F;
    private float sensitivityY = 0.75F;

    private float minimumX = -360F;
    private float maximumX = 360F;

    [SerializeField] float minimumY = -60F;
    [SerializeField] float maximumY = 60F;

    private float rotationY = 0F;
    private float rotationX = 0F;

	//<MOD. ADD>
	Quaternion rot;
	[SerializeField] bool smoothRot;
	//<END MOD>

    protected void Start()
    {
        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;
        originalRotation = transform.localRotation;

		//<MOD. ADD>
			sensitivityX = sensitivityY = .3f;
		//<END MOD>
    }

    protected void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.touches[0];

            if (touch.phase == TouchPhase.Moved)
            {
                rotationX += touch.deltaPosition.x * sensitivityX;
                rotationY += touch.deltaPosition.y * sensitivityY;

                rotationX = ClampAngle(rotationX, minimumX, maximumX);
                rotationY = ClampAngle(rotationY, minimumY, maximumY);

                Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
                Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, -Vector3.right);

                //transform.localRotation = ConvertRotation(referanceRotation * originalRotation * xQuaternion * yQuaternion);

				//<MOD. ADD>
				rot = ConvertRotation(referanceRotation * originalRotation * xQuaternion * yQuaternion);

				if(smoothRot) transform.localRotation = Quaternion.Slerp(transform.localRotation, rot, Time.deltaTime * 8);
				else transform.localRotation = rot;
				//<END MOD>
            }
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }

    private static Quaternion ConvertRotation(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }
}