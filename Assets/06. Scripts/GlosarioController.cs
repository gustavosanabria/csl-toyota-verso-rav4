﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;

public class GlosarioController {
    private XmlDocument _xml;
	private IDictionary<string, string> _dictionary, _defTitleDic, _palabraKeywordDic, _vrTargetsDic, _tutCatDic;

    #region Singleton
    private static GlosarioController _instance;
    public static GlosarioController Instance
    {
        private set { }
        get
        {
            if (_instance == null)
                _instance = new GlosarioController();
            return _instance;
        }
    }
    private GlosarioController() { }
    #endregion Singleton

    public void LoadGlossary(string lang_code)
    {
        string filePath = string.Format("Glosario/{0}", lang_code);
        TextAsset textAsset = (TextAsset)Resources.Load(filePath, typeof(TextAsset));
        _xml = new XmlDocument();
        _xml.LoadXml(textAsset.text);

        XmlNodeList lista = _xml.GetElementsByTagName("record");
        _dictionary = new Dictionary<string, string>(lista.Count);
        _defTitleDic = new Dictionary<string, string>(lista.Count);
        _palabraKeywordDic = new Dictionary<string, string>(lista.Count);
		_vrTargetsDic = new Dictionary<string, string>(lista.Count);	//MOD GLOSSARY V.2
		_tutCatDic = new Dictionary<string, string>(lista.Count);		//MOD GLOSSARY V.2
        foreach (XmlNode node in lista)
        {
            //string palabra = node.FirstChild.InnerText;
            string palabra = node.ChildNodes[0].InnerText;

            string tituloDefinicion = node.ChildNodes[1].InnerText;
            _defTitleDic.Add(palabra, tituloDefinicion);
            //string definicion = node.LastChild.InnerText;

            string definicion = node.ChildNodes[2].InnerText;
            _dictionary.Add(palabra, definicion);

            string keyword = node.ChildNodes[3].InnerText;
            _palabraKeywordDic.Add(palabra, keyword);

			string vrTargets = node.ChildNodes[4].InnerText;	//MOD GLOSSARY V.2
			_vrTargetsDic.Add(palabra, vrTargets);				//MOD GLOSSARY V.2
			
			string tutCat = node.ChildNodes[5].InnerText;	//MOD GLOSSARY V.2
			_tutCatDic.Add(palabra, tutCat);				//MOD GLOSSARY V.2
        }
    }

    public List<string> GetWords()
    {
        return new List<string>(_dictionary.Keys);
    }

    public string GetDefinition(string word)
    {
        return _dictionary[word];
    }

    public string GetDefinitionTitle(string word)
    {
        return _defTitleDic[word];
    }

	public string GetDefinitionTarget(string word)	//MOD GLOSSARY V.2
	{
		return _vrTargetsDic[word];
	}
	
	public string GetDefinitionTut(string word)	//MOD GLOSSARY V.2
	{
		return _tutCatDic[word];
	}

    public List<string> Search(string text)
    {
        string request = text.ToUpper();
        List<string> result = new List<string>();
        /* foreach (string palabra in _dictionary.Keys)
             if (palabra.ToUpper().StartsWith(text.ToUpper()))
             //if (palabra.ToUpper().Contains(text.ToUpper()))
                 result.Add(palabra);*/

        foreach (KeyValuePair<string, string> palDef in _dictionary)
        {
            if ((palDef.Key.ToUpper().Contains(request) || (palDef.Value.ToUpper().Contains(request))) && !result.Contains(palDef.Key))
                result.Add(palDef.Key);
        }

        foreach (KeyValuePair<string, string> palKeyWord in _palabraKeywordDic)
        {
            if (palKeyWord.Value.ToUpper().Contains(request) && !result.Contains(palKeyWord.Key))
                result.Add(palKeyWord.Key);
        }

        //MonoBehaviour.print("DONE");
        return result;
    }

    public void Clear() { _dictionary.Clear(); }
}
