﻿using UnityEngine;
using System.Collections;

public class AppLibrary : MonoBehaviour
{
	public const byte AppVERSO = 0;
	public const byte AppRAV4 = 1;

    public static readonly byte AppTYPE = AppVERSO;
 
    //SCENES<
    public const byte SCENE_SPLASH = 1;

    public const byte SCENE_VERSO_MENU = 2;
    public const byte SCENE_VERSO_AR = 3;
    public const byte SCENE_VERSO_360 = 4;

    public const byte SCENE_AR_HELPVIDEO = 5;
    
	public const byte SCENE_GLOSSARY = 6;

    public const byte SCENE_EMPTY = 7;
    public const byte SCENE_LOADING = 8; 
    //>SCENES

    public const byte VERSO_MARKER_STEERINGWHEEL = 0;
	public const byte VERSO_MARKER_TACHOMETER = 1;
	public const byte VERSO_MARKER_RADIO = 2;
	public const byte RAV4_MARKER_AC = 3;
	public const byte RAV4_MARKER_SW = 4;
	public const byte RAV4_MARKER_TACHOMETER = 5;

	public static byte sceneLoad = 0;

	public static bool openTutAtomatically = false;
	public static bool openTFTTutAtomatically = false;

	public const int TUT_CATEGORY_PHONE = 1;
	public const int TUT_CATEGORY_GPS = 2;
	public const int TUT_CATEGORY_APP = 3;
	public const int TUT_CATEGORY_RADIO = 4;
	public static string TUT_TO_OPEN = "";	//MOD GLOSSARY V.2
	public static string LASTDEF = "";	//MOD GLOSSARY V.2

	public static int TFTSourceScene;

	public static string CURRENTVIDEONAME;
	public static string VIDEO_HELP_AR = "ARHelpVideo.mp4";
	public static string VIDEO_VR = "TireRepairkitFR.mp4";

    public const string AssistancePhone1 = "0800808935";
    public const string AssistancePhone1B = "+33149937226";
    public const string AssistancePhone2 = "0800869682";
    public const string AssistancePhone2B = "+33171041747";

	public static bool AR_HELPVIDEO_PLAYED = false;

	public static string APPKEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj5Km3NvFsC5rx34W98suLLcdeyOgmV3V1nnruGogXuRFCHcWr9eAaaFhXCaenx1f1a/CqwV5Vz88cRZKVToMYGJe9+ifc8lulrbtoStEPwqCb7p/AQwOMZzxEeYU7nYhHFx+ssx/NCwoDL047rcbG4KweUQA0Irl6fJTkfM30rCCxLo3ZN4szlU7OifHwbzrJErf7C/qTNA31nFjUsSs05Wmj5dGlR9yOxA7OA3Zf8CD/lsJuYYZJ/kQxZd3jn5+OsAyiI8o5V6vBD29Ah6Apwm4yvfbdwog2EWFRh9Y9GonyZ2HTwj3tW1IiSsXDQWXS+GjUx7pwCRDsPSkiI+ruQIDAQAB";
}
