﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlossaryMgr : MonoBehaviour
{
	[SerializeField] UITexture definitionTexture;
	[SerializeField] GameObject wordsContainer;
	string path = "Glosario/Textures/Definitions";
    [SerializeField] UILabel defTitle, def;

	[SerializeField] GameObject[] wordsButtons;
	int btnCount;
	[SerializeField] UIButton backToMenuBtn;	//MOD GLOSSARY V.2
	public static List<string> vrTargetsToShow = new List<string>();	//MOD GLOSSARY V.2
	public static string LASTDEFWORD = "";	//MOD GLOSSARY V.2
	
	public GameObject showInVRBtn;	//MOD GLOSSARY V.2
	public GameObject showInTUTBtn;	//MOD GLOSSARY V.2

	void Awake()
	{
		Resources.UnloadUnusedAssets();
		System.GC.Collect();

        GetComponent<GlosarioUIController>().Load("fr2");

		vrTargetsToShow.Clear();		//MOD GLOSSARY V.2
		AppLibrary.TUT_TO_OPEN = "";	//MOD GLOSSARY V.2
	}

	void EnableButtons(bool b)
	{
		wordsButtons = GameObject.FindGameObjectsWithTag("wordBtn");
		btnCount = wordsButtons.Length;
		for (int i = 0; i < btnCount; i++)
		{
			wordsButtons[i].GetComponent<UIButton>().enabled = b;
		}
	}

	string vrTarget;	//MOD GLOSSARY V.2
	public void GetDefinition(string pDefTitle, string pDef,string pVrTarget, string pTut)
	{
		EnableButtons(false);
		vrTarget = pVrTarget;	//MOD GLOSSARY V.2
		AppLibrary.TUT_TO_OPEN = pTut;		//MOD GLOSSARY V.2
		SetVrTargets();			//MOD GLOSSARY V.2
		
		//print(vrTarget +" .Cat:" + AppLibrary.TUT_TO_OPEN);
		
		showInVRBtn.SetActive(vrTarget.Length > 0);
		showInTUTBtn.SetActive(AppLibrary.TUT_TO_OPEN.Length > 0);

		StartCoroutine(ShowDefinition(pDefTitle, pDef));
	}

	public void CloseDefinition()
	{
		vrTargetsToShow.Clear();	//MOD GLOSSARY V.2
		AppLibrary.TUT_TO_OPEN = "";	//MOD GLOSSARY V.2
		LASTDEFWORD = "";	//MOD GLOSSARY V.2

		definitionTexture.GetComponent<TweenScale>().PlayReverse();
		wordsContainer.SetActive(true);
		backToMenuBtn.gameObject.SetActive(true);	//MOD GLOSSARY V.2
		EnableButtons(true);
	}

	string lastDef, fullPath;
	IEnumerator ShowDefinition(string pDefTitle, string pDef)
	{
		//if(lastDef != pDefTitle)
		//{
            //Resources.UnloadAsset (definitionTexture.mainTexture);
            //Resources.UnloadUnusedAssets();

            //fullPath = string.Format("{0}/{1}", path, pDef);

            //ResourceRequest resourceRequest = Resources.LoadAsync<Texture2D>(fullPath);
            //yield return resourceRequest;

            //definitionTexture.mainTexture = (Texture2D)resourceRequest.asset;

            defTitle.enabled = def.enabled = false;
            defTitle.text = pDefTitle;
            def.text = pDef;
            defTitle.enabled = def.enabled = true;

            defTitle.alignment = def.alignment = NGUIText.Alignment.Center;
            //lastDef = pDefTitle;
        //}
		
		yield return new WaitForSeconds(0.2f);
		backToMenuBtn.gameObject.SetActive(false);	//MOD GLOSSARY V.2
		wordsContainer.SetActive(false);
		definitionTexture.GetComponent<TweenScale>().PlayForward();
	}

    void FixedUpdate()
    {
        if (!wordsContainer.activeInHierarchy)
        {
            defTitle.alignment = def.alignment = NGUIText.Alignment.Automatic;
            defTitle.alignment = def.alignment = NGUIText.Alignment.Center;
        }
    }

	void SetVrTargets()	//MOD GLOSSARY V.2
	{
		vrTargetsToShow.Clear();
		
		string[] targets = vrTarget.Split(',');
		
		foreach (string targetID in targets)
		{
			if (targetID.Length > 0)
			{
				vrTargetsToShow.Add (targetID);
				//print (targetID);
			}
		}
	}
	
	public void ShowInVR()	//MOD GLOSSARY V.2
	{
		AppLibrary.TUT_TO_OPEN = "";
		MySceneManager.JumpFromGlossaryTo360();
	}
	
	public void ShowInTutorial()	//MOD GLOSSARY V.2
	{
		MySceneManager.JumpFromGlossaryToTutCategory();
	}
}
