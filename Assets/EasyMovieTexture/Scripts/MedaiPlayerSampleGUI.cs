using UnityEngine;
using System.Collections;

public class MedaiPlayerSampleGUI : MonoBehaviour {

	[SerializeField] GameObject loadingGO;
	public MediaPlayerCtrl scrMedia;
	
	public bool m_bFinish = false;
	// Use this for initialization
	void Start () {
		scrMedia.OnEnd += OnEnd;
#if UNITY_EDITOR
        OnEnd();
#endif
	}

	void OnEnd()
	{
		m_bFinish = true;

		if (scrMedia.isHelpVideo)
		{
            //scrMedia.Stop();
            //scrMedia.Call_Reset();
            //gameObject.SetActive(false);

			if(scrMedia.m_strFileName == AppLibrary.VIDEO_HELP_AR)
			{
				AppLibrary.AR_HELPVIDEO_PLAYED = true;
				Application.LoadLevel(AppLibrary.SCENE_VERSO_MENU);
				if(scrMedia.hSM) scrMedia.hSM.EnableHomeUICamera(true);
			}
			else AppLibrary.AR_HELPVIDEO_PLAYED = false;
            
            //Application.UnloadLevel(AppLibrary.SCENE_AR_HELPVIDEO);
			if(scrMedia.m_strFileName == AppLibrary.VIDEO_VR)
			{
				//GameObject.Find("Manager").GetComponent<DescriptionsManager>().ShowTargets();
				if(loadingGO){loadingGO.SetActive(true);}
				Application.LoadLevel(AppLibrary.SCENE_VERSO_360);
			}

            //GameObject[] gos = GameObject.FindGameObjectsWithTag("Additive Asset");

            //for (int i = 0; i < gos.Length; i++)
            //{
               // Destroy(gos[i]);
            //}

            //print("Additive assets destroyed");
            Resources.UnloadUnusedAssets();
			System.GC.Collect();
		}
		else MySceneManager.GoToHomeScreen(true);
	}
}
